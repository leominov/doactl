package restore

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"regexp"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/backup"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/createdatabase"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropconnections"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropdatabase"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/facts"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/reassign"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/renamedatabase"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/restore/list"
)

// Options настройки операции восстановления из резервной копии
type Options struct {
	// Database описание настроек базы данных
	Database postgres.Database
	// Schema объекты которой мы восстанавливаем. По умолчанию ВСЕ схемы.
	Schema string
	// Bucket имя бакета, где хранятся бэкапы
	Bucket string
	// Instance имя инстанса PostgreSQL
	Instance string
	// Backup информация о бэкапе с которого нужно производить
	// восстановление
	Backup *backups.Backup
	// Owner пользователь на которого будет назначена база данных
	Owner string
	// DropBefore удаляет базу данных перед восстановлением
	DropBefore bool
	// UseTempDatabase для восстановления данных используется
	// временная база данных, которой будет заменена оригинальная
	UseTempDatabase bool
	// UseListExclude используется для исключения контента из карты
	// резервной копии, для этого сначала вызывается команда:
	// $ pg_restore -l -f out.txt backup.dump
	// Затем контент записывается согласно пользовательскому списку
	// исключений, каждая из записей представляет собой регулярное
	// выражение
	UseListExclude []*regexp.Regexp
}

// Сначала пытаемся получить факты подключаясь к конкретной базе данных,
// затем, если подключение завершилось ошибкой, подключаемся без указания.
//
// Таким образом получаем необходимые данных о версии и о наличии базы,
// которую собираемся восстанавливать.
func collectRemoteFacts(opts *facts.Options) (bool, *facts.Facts, error) {
	factsRemote, err := facts.DoRemove(opts)
	if err == nil {
		return true, factsRemote, err
	}
	opts.Database.Name = ""
	factsRemote, err = facts.DoRemove(opts)
	if err != nil {
		return false, nil, err
	}
	return false, factsRemote, err
}

// По резервной копии составляем карту контента, далее используя пользовательские
// фильтры исключаем из списка ненужное, сохраняем на диск и возвращаем в ответ
// расположение файла для использования pg_restore
func createUseListFile(opts *Options, dumpFilename string) (string, error) {
	l, err := list.Contents(dumpFilename)
	if err != nil {
		return "", err
	}

	l.MassFilter(opts.UseListExclude)

	baseDir := path.Dir(dumpFilename)
	useListFilename := path.Join(baseDir, opts.Backup.File+".use-list")
	ulf, err := os.Create(useListFilename)
	if err != nil {
		return "", err
	}
	defer ulf.Close()

	_, err = ulf.Write(l.Bytes())
	if err != nil {
		return "", err
	}
	return useListFilename, nil
}

// RunStandardStrategy стратегия восстановления данных, используемая
// по умолчанию, восстанавливая база данных может быть пересозадана,
// если требуется или очищена перед восстановлением
func RunStandardStrategy(opt *Options, exists bool) error {
	// pg_restore не работает со строковыми резервными копиями,
	// если ему передать такую копию, то он выдаст ошибку
	bin, err := exec.LookPath("pg_restore")
	if err != nil {
		return nil
	}

	if opt.DropBefore {
		// Пытаемся решить проблему при восстановлении данных. Лог ошибки:
		//  > pg_restore: [archiver (db)] Error while PROCESSING TOC:
		//  > pg_restore: [archiver (db)] Error from TOC entry 190; 1259 153529 SEQUENCE menu_item_legacy_id pfa_routes_menu
		//  > pg_restore: [archiver (db)] could not execute query: ERROR:  cannot drop sequence public.menu_item_legacy_id because other objects depend on it
		//  > DETAIL:  default for table public.menu_item_legacy column legacyMenuItemId depends on sequence public.menu_item_legacy_id
		//  > HINT:  Use DROP ... CASCADE to drop the dependent objects too.
		//    Command was: DROP SEQUENCE IF EXISTS public.menu_item_legacy_id;
		// База при этом существует, а сам pg_restore запускается с флагом `--clean`
		dropdatabase.Do(&dropdatabase.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		})
		exists = false
	}

	storage, err := backups.Open(opt.Bucket)
	if err != nil {
		return err
	}
	defer storage.Close()

	// Мы не можем писать в pg_restore набор байт в stdin, поэтому сначала
	// сохраняем резервную копию во временный каталог, это будет полезно в
	// случае, когда будет использоваться barrier для работы с резервными
	// копиями
	tmpDir, err := ioutil.TempDir("", "restore")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmpDir)

	dumpFilename := path.Join(tmpDir, opt.Backup.File)
	err = storage.Download(opt.Backup.File, dumpFilename)
	if err != nil {
		return err
	}

	// Исключаем из процедуры восстановления ненужный контент
	var useListFilename string
	if len(opt.UseListExclude) > 0 {
		useListFilename, err = createUseListFile(opt, dumpFilename)
		if err != nil {
			return err
		}
	}

	if exists {
		// Обрываем все открытые соединения с базой данных, процедура не
		// требуется, если база создается при восстановлении
		dropConnectionsOptions := &dropconnections.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		}
		_, err := dropconnections.Do(dropConnectionsOptions)
		if err != nil {
			return err
		}
	}

	var args []string
	changeOwner := len(opt.Owner) > 0 && opt.Owner != opt.Database.User.Name

	// Флаги, которые нужно использовать, если дамп накатывается на инстанс,
	// в котором отсутствует восстанавливаемая база данных:
	// * --create
	// * --dbname=postgres
	// В противном случае:
	// * --clean
	// * --if-exists
	// * --dbname=name
	if exists {
		args = append(args, []string{
			"--clean",
			"--if-exists",
			fmt.Sprintf("--dbname=%s", opt.Database.Name),
		}...)
	} else {
		args = append(args, []string{
			"--create",
			"--dbname=postgres",
		}...)
	}

	if opt.Schema != "" {
		args = append(args, fmt.Sprintf("--schema=%s", opt.Schema))
	}

	if len(useListFilename) > 0 {
		args = append(args, fmt.Sprintf("--use-list=%s", useListFilename))
	}

	args = append(args, []string{
		"--no-owner",
		"--no-privileges",
		"--no-password",
		"--exit-on-error",
		fmt.Sprintf("--format=%s", backup.CustomFormat.String()),
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.Database.User.Name),
		dumpFilename,
	}...)

	envs := []string{
		fmt.Sprintf("PGPASSWORD=%s", opt.Database.User.Password),
	}

	cmd := exec.Command(bin, args...)
	cmd.Env = envs
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	if changeOwner {
		// Меняем владельца, если требуется и если имя не совпадает с
		// тем которое использовалось при восстановлении данных
		reassignOptions := &reassign.Options{
			Instance: opt.Instance,
			Owner:    opt.Owner,
			Database: opt.Database,
		}
		err := reassign.Do(reassignOptions)
		if err != nil {
			return err
		}
	}
	return nil
}

// RunTempDatabaseStrategy стратегия восстановления данных, использующая
// временную базу данных, последним шагом полученная база данных будет
// переименована в восстанавливаемую
func RunTempDatabaseStrategy(opt *Options, exists bool) error {
	tempDatabaseName := fmt.Sprintf("_tmp_tools_%s", opt.Database.Name)

	bin, err := exec.LookPath("pg_restore")
	if err != nil {
		return nil
	}

	// Всегда пытаемся удалить временную базу данных, которая могла
	// существовать по каким-либо причинам
	err = dropdatabase.Do(&dropdatabase.Options{
		Instance: opt.Instance,
		Database: postgres.Database{
			Name: tempDatabaseName,
			User: opt.Database.User,
		},
	})
	if err != nil {
		return nil
	}

	// Создаем пустую базу данных, куда будут восстановлены данные
	err = createdatabase.Do(&createdatabase.Options{
		Instance: opt.Instance,
		Database: postgres.Database{
			Name: tempDatabaseName,
			User: opt.Database.User,
		},
	})
	if err != nil {
		return nil
	}

	storage, err := backups.Open(opt.Bucket)
	if err != nil {
		return err
	}
	defer storage.Close()

	tmpDir, err := ioutil.TempDir("", "restore")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmpDir)

	dumpFilename := path.Join(tmpDir, opt.Backup.File)
	err = storage.Download(opt.Backup.File, dumpFilename)
	if err != nil {
		return err
	}

	// Исключаем из процедуры восстановления ненужный контент
	var useListFilename string
	if len(opt.UseListExclude) > 0 {
		useListFilename, err = createUseListFile(opt, dumpFilename)
		if err != nil {
			return err
		}
	}

	// Флаг `--clean` пропускаем, так как база была создана и
	// полагаем, что она все еще пуста
	args := []string{
		"--no-owner",
		"--no-privileges",
		"--no-password",
		"--exit-on-error",
		"--no-tablespaces",
		fmt.Sprintf("--dbname=%s", tempDatabaseName),
		fmt.Sprintf("--format=%s", backup.CustomFormat.String()),
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.Database.User.Name),
		dumpFilename,
	}

	if opt.Schema != "" {
		args = append(args, fmt.Sprintf("--schema=%s", opt.Schema))
	}

	if len(useListFilename) > 0 {
		args = append(args, fmt.Sprintf("--use-list=%s", useListFilename))
	}

	envs := []string{
		fmt.Sprintf("PGPASSWORD=%s", opt.Database.User.Password),
	}

	cmd := exec.Command(bin, args...)
	cmd.Env = envs
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	if len(opt.Owner) > 0 && opt.Owner != opt.Database.User.Name {
		// Меняем владельца у временной базы данных в соответствии
		// с переданными настройками
		reassignOptions := &reassign.Options{
			Instance: opt.Instance,
			Owner:    opt.Owner,
			Database: postgres.Database{
				Name: tempDatabaseName,
				User: opt.Database.User,
			},
		}
		err := reassign.Do(reassignOptions)
		if err != nil {
			return err
		}
	}

	if exists {
		// Обрываем все открытые соединения с базой данных, процедура не
		// требуется, если база не существовала на момент запуска
		dropConnectionsOptions := &dropconnections.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		}
		_, err := dropconnections.Do(dropConnectionsOptions)
		if err != nil {
			return err
		}
		// Удаляем существующую базу данных со всем содержимым
		err = dropdatabase.Do(&dropdatabase.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		})
		if err != nil {
			return nil
		}
	}

	// Переименовываем временную базу данных на оригинальное название
	err = renamedatabase.Do(&renamedatabase.Options{
		Instance: opt.Instance,
		RenameTo: opt.Database.Name,
		Database: postgres.Database{
			Name: tempDatabaseName,
			User: opt.Database.User,
		},
	})
	if err != nil {
		return nil
	}

	return nil
}

func getBackupFileFormat(opt *Options) (backup.Format, error) {
	formatRaw := opt.Backup.GetMetadata(backup.PGDumpFormatMeta)
	if len(formatRaw) == 0 {
		return backup.CustomFormat, errors.New("backup file format not defined in meta")
	}
	format, err := backup.ParseFormat(formatRaw)
	if err != nil {
		return backup.CustomFormat, err
	}
	return format, err
}

func RunPlainStrategy(opt *Options, exists bool) error {
	// psql используется для восстановления данных из текстового
	// формата резервной копии
	bin, err := exec.LookPath("psql")
	if err != nil {
		return nil
	}

	if opt.DropBefore {
		err := dropdatabase.Do(&dropdatabase.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		})
		if err != nil {
			return err
		}
		err = createdatabase.Do(&createdatabase.Options{
			Instance: opt.Instance,
			Database: postgres.Database{
				Name: opt.Database.Name,
				User: opt.Database.User,
			},
		})
		if err != nil {
			return err
		}
	}

	storage, err := backups.Open(opt.Bucket)
	if err != nil {
		return err
	}
	defer storage.Close()

	// TODO(l.aminov): Лить в stdin psql
	tmpDir, err := ioutil.TempDir("", "restore")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmpDir)

	dumpFilename := path.Join(tmpDir, opt.Backup.File)
	err = storage.Download(opt.Backup.File, dumpFilename)
	if err != nil {
		return err
	}

	if exists {
		// Обрываем все открытые соединения с базой данных, процедура не
		// требуется, если база создается при восстановлении
		dropConnectionsOptions := &dropconnections.Options{
			Instance: opt.Instance,
			Database: opt.Database,
		}
		_, err := dropconnections.Do(dropConnectionsOptions)
		if err != nil {
			return err
		}
	}

	changeOwner := len(opt.Owner) > 0 && opt.Owner != opt.Database.User.Name

	args := []string{
		"--quiet",
		"--echo-errors",
		"--no-password",
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.Database.User.Name),
		fmt.Sprintf("--dbname=%s", opt.Database.Name),
		fmt.Sprintf("--file=%s", dumpFilename),
	}

	envs := []string{
		fmt.Sprintf("PGPASSWORD=%s", opt.Database.User.Password),
	}

	cmd := exec.Command(bin, args...)
	cmd.Env = envs
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	if changeOwner {
		// Меняем владельца, если требуется и если имя не совпадает с
		// тем которое использовалось при восстановлении данных
		reassignOptions := &reassign.Options{
			Instance: opt.Instance,
			Owner:    opt.Owner,
			Database: opt.Database,
		}
		err := reassign.Do(reassignOptions)
		if err != nil {
			return err
		}
	}

	return nil
}

// Do запустить операцию восстановления данных из резервной копии
func Do(opt *Options) error {
	factsLocal, err := facts.DoLocal()
	if err != nil {
		return err
	}

	exists, factsRemote, err := collectRemoteFacts(&facts.Options{
		Instance: opt.Instance,
		Database: postgres.Database{
			Name: opt.Database.Name,
			User: opt.Database.User,
		},
	})
	if err != nil {
		return err
	}

	// Выводим ошибку, если локальная версия PostgreSQL отличается той,
	// к которой будет подключаться для восстановления данных
	err = facts.CompareFacts(factsLocal, factsRemote)
	if err != nil {
		return err
	}

	// Получаем информацию о формате восстанавливаемой копии, от этого зависит
	// с помощью каких инструментов будет происходит восстановление
	format, err := getBackupFileFormat(opt)
	if err != nil {
		return fmt.Errorf("failed to get backup file format: %v", err)
	}

	// Логика восстановления данных реализована в двух вариантах:
	// * прямое восстановление данных БД из резервной копии в формате Plain.
	// * прямое восстановление данных БД из резервной копии в формате Custom;
	// * через создание временной БД, резервная копия в формате Custom;
	if format == backup.PlainFormat {
		return RunPlainStrategy(opt, exists)
	}
	if !opt.UseTempDatabase {
		return RunStandardStrategy(opt, exists)
	}

	return RunTempDatabaseStrategy(opt, exists)
}
