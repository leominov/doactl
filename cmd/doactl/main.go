package main

import (
	"fmt"
	"os"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/commands/completion"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/spinner"
)

const (
	// CommonUserList хранилище учетных записей
	CommonUserList = "vault://advert/gcp/pfa-prod/cloudsql/production/users/"
	// CommonPrefix префикс для списка пользователей и резервных копий
	CommonPrefix = "pfa_"
	// CommonInstance инстанс для работы по умолчанию
	CommonInstance = "localhost"
	// NextMajorDeprecatedNotice стандартная аргументация при использовании устаревающих флагов,
	// с маленькой буквы, потому что в начала будет стандартный текст, пример целиком:
	// Flag --skip-compare-facts has been deprecated, will be removed in next major release
	NextMajorDeprecatedNotice = "will be removed in next major release"
)

var (
	// Spinner используется для индикации загрузки
	Spinner = spinner.New(100 * time.Millisecond)
)

func rootCmd(_ []string) *cobra.Command {
	rootCmd := &cobra.Command{
		Use:          "doactl",
		Short:        "DevOps Advert Toolkit",
		SilenceUsage: true,
	}

	out := rootCmd.OutOrStdout()

	rootCmd.AddCommand(
		completion.NewCmd(out),
		pgCmd(out),
		versionCmd(out),
		psqlCmd(out),
		secretsCmd(out),
		networkCmd(out),
	)

	return rootCmd
}

func main() {
	cmd := rootCmd(os.Args[1:])
	if err := Initialize(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if err := cmd.Execute(); err != nil {
		os.Exit(1)
	}
}
