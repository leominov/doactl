package postgres

import (
	"net/url"
	"os"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

const (
	// DefaultInstance используется как значение по умолчанию
	DefaultInstance = "localhost"
	// DefaultUser используется как значение по умолчанию
	DefaultUser = "postgres"
	// DefaultPassword используется как значение по умолчанию
	DefaultPassword = "postgres"
)

// Database описание подключения к базе данных
type Database struct {
	// Имя базы данных, в случае работы приложения PFA, всегда
	// соответствует имени пользователя
	Name string
	// Описание реквизитов пользователя
	User *users.User
}

// URL возвращает url.URL представление для подключения к БД
//
// Так как частр аргументов не передается через параметры запуска,
// например, пароль подключения, собираем url.URL самостоятельно
func (d *Database) URL(instance string) *url.URL {
	u := new(url.URL)
	u.Scheme = "postgres"
	u.Host = instance

	if len(d.Name) > 0 {
		u.Path = "/" + d.Name
	}

	if d.User != nil {
		u.User = url.UserPassword(d.User.Name, d.User.Password)
	}

	query := u.Query()
	// Нет возможности передать аргументы напрямую, поэтому
	// предполагаем, что sslmode всегда должен быть со значение
	// `disable`, чтобы избежать возможные ошибки
	query.Set("sslmode", "disable")
	u.RawQuery = query.Encode()

	return u
}

func UserFromEnv() *users.User {
	u := &users.User{
		Name:     DefaultUser,
		Password: DefaultPassword,
	}

	name := os.Getenv("PGUSER")
	if len(name) > 0 {
		u.Name = name
	}

	pass := os.Getenv("PGPASSWORD")
	if len(pass) > 0 {
		u.Password = pass
	}

	return u
}
