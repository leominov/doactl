#!/usr/bin/env bash

set -uo pipefail

echo "Starting PostgreSQL Server..."
docker rm -f dev-postgres96 &> /dev/null || :
if ! docker run --cap-add=IPC_LOCK -d --name=dev-postgres96 -p 5432:5432 postgres:9.6 &> /dev/null ; then
  echo "Failed to start dev-postgres96"
  exit 1
fi
echo "...done. Run \"docker rm -f dev-postgres96\" to clean up the container."
echo
