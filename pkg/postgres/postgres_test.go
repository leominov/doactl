package postgres

import (
	"testing"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

func TestURL(t *testing.T) {
	tests := []struct {
		db       *Database
		instanse string
		want     string
	}{
		{
			db:       &Database{},
			instanse: "127.0.0.1",
			want:     "postgres://127.0.0.1?sslmode=disable",
		},
		{
			db: &Database{
				Name: "dbname",
			},
			instanse: "127.0.0.1",
			want:     "postgres://127.0.0.1/dbname?sslmode=disable",
		},
		{
			db: &Database{
				Name: "dbname",
				User: &users.User{
					Name: "login",
				},
			},
			instanse: "127.0.0.1",
			want:     "postgres://login:@127.0.0.1/dbname?sslmode=disable",
		},
		{
			db: &Database{
				Name: "dbname",
				User: &users.User{
					Name:     "login",
					Password: "password",
				},
			},
			instanse: "127.0.0.1",
			want:     "postgres://login:password@127.0.0.1/dbname?sslmode=disable",
		},
	}
	for _, test := range tests {
		u := test.db.URL(test.instanse)
		if u.String() != test.want {
			t.Errorf("db.URL() = %s, want = %s", u.String(), test.want)
		}
	}
}
