package backups

import "gocloud.dev/blob"

type FileAttrs struct {
	CacheControl       string            `json:"user.cache_control"`
	ContentDisposition string            `json:"user.content_disposition"`
	ContentEncoding    string            `json:"user.content_encoding"`
	ContentLanguage    string            `json:"user.content_language"`
	ContentType        string            `json:"user.content_type"`
	Metadata           map[string]string `json:"user.metadata"`
	MD5                []byte            `json:"md5"`
}

// BlobAttributesToFileAttributes стандартные поля blob.Attributes энкодятся в публичные
// поля, которые не используются для внутреннего хранилища, поэтому для организации обратной
// совместимости, аттрибуты блоба переводим в аттрибуты файла
// ref: gocloud.dev/blob/fileblob/attrs.go
func BlobAttributesToFileAttributes(attr *blob.Attributes) *FileAttrs {
	return &FileAttrs{
		CacheControl:       attr.CacheControl,
		ContentDisposition: attr.ContentDisposition,
		ContentEncoding:    attr.ContentEncoding,
		ContentLanguage:    attr.ContentLanguage,
		ContentType:        attr.ContentType,
		Metadata:           attr.Metadata,
		MD5:                attr.MD5,
	}
}
