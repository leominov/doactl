package main

import (
	"io"

	"github.com/spf13/cobra"
)

var (
	// NoHeaders не выводить заголовок таблицы
	NoHeaders bool
)

func pgCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "pg",
		Short: "Manage PostgreSQL databases",
	}

	cmd.AddCommand(
		pgBackupCmd(out),
		pgCredentialsCmd(out),
		pgPingCmd(out),
		pgDropConnectionsCmd(out),
		pgDropDatabaseCmd(out),
		pgListDatabasesCmd(out),
		pgDebugToolsCmd(out),
	)

	cmd.PersistentFlags().BoolVar(&NoHeaders, "no-headers", false, "When using the custom-column output format, don't print headers")

	return cmd
}
