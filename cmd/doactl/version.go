package main

import (
	"bytes"
	"context"
	"fmt"
	"io"

	"github.com/spf13/cobra"
	"gocloud.dev/gcp"
)

var (
	// Version будет перезаписана goreleaser
	Version string
)

func GetVersion() string {
	if len(Version) == 0 {
		return "X.X"
	}
	return Version
}

func versionCmd(out io.Writer) *cobra.Command {
	var printsCredentialsInfo bool
	cmd := &cobra.Command{
		Use:   "version",
		Short: "Prints version and exit",
		RunE: func(_ *cobra.Command, _ []string) error {
			fmt.Fprintln(out, GetVersion())
			if !printsCredentialsInfo {
				return nil
			}
			credentials, err := gcp.DefaultCredentials(context.Background())
			if err != nil {
				return err
			}
			fmt.Fprintf(out, "Google Project ID: %s\n", credentials.ProjectID)
			fmt.Fprintf(out, "Google JSON Key: %s\n", bytes.TrimSpace(credentials.JSON))
			return nil
		},
	}
	cmd.Flags().BoolVar(&printsCredentialsInfo, "extended", false, "Prints information about credentials")
	return cmd
}
