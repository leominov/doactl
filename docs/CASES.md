# Cases

## Развернуть локально копию базы данных pfa_auth

С версии 5.8.0 doactl содержит Service Account для доступа на чтение к хранилищу резервных копий баз данных. Если требуется указать Service Account с дополнительными привилегиями, используйте переменную `GOOGLE_APPLICATION_CREDENTIALS`, в качестве значения которой следует задать путь к файлу.

```shell
# Смотрим список доступных резервных копий
$ doactl pg backup list --latest --bucket=gs://pfa-postgres-dump-compressed/ --prefix=pfa_auth
Database  Backup ID              Size    Date
pfa_auth  pfa_auth_20191010.sql  192 kB  20191010
# Восстанавливаем данные
$ doactl pg backup restore pfa_auth --local --bucket=gs://pfa-postgres-dump-compressed/ --date=20191010
```

## Восстановить базу данных pfa_apps на QA-окружении в GKE

```shell
# Запускаем деплоймент с инструментарием в нужном окружении и заходим в созданный контейнер
$ kubectl -n qa2 run doactl --rm -it --image=eu.gcr.io/utilities-212509/devops/doactl:9.6-5.4.0 --command -- bash
# Восстанавливаем данные; будет использована резервная копия за текущий день
$ doactl pg backup restore pfa_apps --local --instance=postgres --bucket=gs://pfa-postgres-dump-compressed/
```

## Создать резервную копию локальной базы данных pfa_auth и загрузить в S3

Убедитесь, что у вас есть доступ к [MinIO](https://minio.tcsbank.ru:9000/).

```shell
$ export AWS_ACCESS_KEY_ID=XXX
$ export AWS_SECRET_ACCESS_KEY=XXX+XXX
$ doactl pg backup create pfa_auth --local --bucket="s3://test?region=eu-west-1&endpoint=https://minio.tcsbank.ru:9000&s3ForcePathStyle=true"
```

Ниже – альтернативный вариант без передачи переменных окружения. Создаем файл `~/.aws/config` с содержимым:

```
[default]
aws_access_key_id=XXX
aws_secret_access_key=XXX+XXX
region=eu-west-1
```

Создаем резервную копию:

```shell
$ doactl pg backup create pfa_auth --local --bucket="s3://test?endpoint=https://minio.tcsbank.ru:9000&s3ForcePathStyle=true"
```
