package retry

import (
	"errors"
	"testing"
	"time"
)

func TestString(t *testing.T) {
	tests := []struct {
		res   string
		stats *Stats
	}{
		{
			res: "Attempt 0/0",
			stats: &Stats{
				Attempt: 0,
				Config: &Config{
					Forever:  false,
					Interval: 0,
				},
			},
		},
		{
			res: "Attempt 1/∞ Retrying in 1s",
			stats: &Stats{
				Attempt:  1,
				Interval: time.Second,
				Config: &Config{
					Forever:  true,
					Interval: time.Second,
				},
			},
		},
	}
	for _, test := range tests {
		if test.stats.String() != test.res {
			t.Errorf("Must be %s, but got %s", test.res, test.stats.String())
		}
	}
}

func TestRetry(t *testing.T) {
	tests := []*Config{
		nil,
		{
			Maximum:  0,
			Forever:  false,
			Interval: 100 * time.Microsecond,
		},
		{
			Increment: true,
		},
		{
			Increment:       true,
			Maximum:         10,
			Interval:        100 * time.Microsecond,
			IntervalMaximum: 50 * time.Microsecond,
			Jitter:          true,
		},
	}
	for _, test := range tests {
		Do(func(s *Stats) error {
			return nil
		}, test)
	}
	err := Do(func(s *Stats) error {
		return nil
	}, &Config{
		Interval: 0,
		Forever:  true,
	})
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	Do(func(s *Stats) error {
		if s.Attempt > 3 {
			s.Break()
		}
		return errors.New("error")
	}, &Config{
		Interval: 100 * time.Microsecond,
		Forever:  true,
	})
	err = Do(func(s *Stats) error {
		return errors.New("error")
	}, &Config{
		Interval: 100 * time.Microsecond,
		Maximum:  3,
	})
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
}
