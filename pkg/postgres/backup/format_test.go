package backup

import "testing"

func TestParseFormat(t *testing.T) {
	tests := []struct {
		in      string
		out     Format
		wantErr bool
	}{
		{
			in:  "plain",
			out: PlainFormat,
		},
		{
			in:  "custom",
			out: CustomFormat,
		},
		{
			in:      "foobar",
			wantErr: true,
		},
	}
	for _, test := range tests {
		f, err := ParseFormat(test.in)
		if test.wantErr {
			if err == nil {
				t.Error("Must be an error, but got nil")
			}
			continue
		}
		if f != test.out {
			t.Errorf("Must be %s, but got %s", test.out, f)
		}
	}
}
