package main

import (
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func TestInitialize_gcloud(t *testing.T) {
	os.Clearenv()
	dir, err := ioutil.TempDir("", "gcp")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)
	err = initializeGCloud(&InitGCloudParams{
		WorkDir:    dir,
		EncodedKey: googleApplicationDefaultCredentials,
	})
	if err != nil {
		t.Fatal(err)
	}
	filename := path.Join(dir, ".config", "doactl", "application_default_credentials.json")
	_, err = os.Stat(filename)
	if err != nil {
		t.Fatal(err)
	}
	envVal := os.Getenv("GOOGLE_APPLICATION_CREDENTIALS")
	if envVal != filename {
		t.Errorf("Must be %q, but got %q", filename, envVal)
	}
}

func TestInitialize_minIO(t *testing.T) {
	os.Clearenv()
	err := initializeMinIO(&InitMinIOParams{
		EncodedSecretKey: minIOSecretKey,
		EncodedAccessKey: minIOAccessKey,
	})
	if err != nil {
		t.Error(err)
	}
}
