package env

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets"
)

// Env реализует поддержку хранения секретов в переменных
// окружения
type Env struct {
}

func init() {
	e := &Env{}
	secrets.RegisterSecret("env", e)
}

// ListPath получение списка секретов по указанному пути
func (e *Env) ListPath(path string) ([]string, error) {
	var result []string

	environ := os.Environ()
	for _, env := range environ {
		data := strings.SplitN(env, "=", 2)
		if len(data) != 2 {
			continue
		}
		if !strings.HasPrefix(data[0], path) {
			continue
		}
		result = append(result, data[0])
	}

	return result, nil
}

// GetPath получение информации по указанному секрету
//
// Переменные окружения не поддерживают вложенные стркуткры, поэтому
// если будет передан путь в формате аналогичный Vault, то из пути
// будет взят только последний элемент.
//
// Получаемые переменные окружения должны храниться в Base64 и в
// формате JSON.
func (e *Env) GetPath(path string) (map[string]string, error) {
	// Env doesn't support nested sets
	path = filepath.Base(path)

	val := os.Getenv(path)
	data, err := base64.StdEncoding.DecodeString(val)
	if err != nil {
		return nil, err
	}

	result := map[string]string{}
	err = json.Unmarshal(data, &result)
	if err != nil {
		return nil, fmt.Errorf("unable to unmarshal '%s' data: %v", path, err)
	}

	return result, nil
}
