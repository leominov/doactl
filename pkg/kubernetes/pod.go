package kubernetes

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"syscall"
)

type DeletePodOptions struct {
	Namespace string
}

func DeletePod(name string, opts *DeletePodOptions) error {
	bin, err := exec.LookPath("kubectl")
	if err != nil {
		return err
	}

	namespace := "default"
	if len(opts.Namespace) > 0 {
		namespace = opts.Namespace
	}

	args := []string{
		"delete",
		"pod",
		fmt.Sprintf("--namespace=%s", namespace),
		name,
	}
	bytes, err := exec.Command(bin, args...).CombinedOutput()
	if err != nil {
		if len(bytes) == 0 {
			return err
		}
		return fmt.Errorf("%s: %v", string(bytes), err)
	}
	return nil
}

type RunPodOptions struct {
	Namespace string
	ImageName string
	Limits    string
	Requests  string
	Command   []string
}

func RunPod(name string, opts *RunPodOptions) error {
	bin, err := exec.LookPath("kubectl")
	if err != nil {
		return err
	}

	if len(opts.ImageName) == 0 {
		return errors.New("image name must be specified")
	}

	namespace := "default"
	if len(opts.Namespace) > 0 {
		namespace = opts.Namespace
	}

	limits := "cpu=100m,memory=128Mi"
	if len(opts.Limits) > 0 {
		limits = opts.Limits
	}

	requests := "cpu=50m,memory=64Mi"
	if len(opts.Requests) > 0 {
		requests = opts.Requests
	}

	command := []string{"bash"}
	if len(opts.Command) > 0 {
		command = opts.Command
	}

	labelSet := []string{
		"app.kubernetes.io/managed-by=doactl",
	}

	args := []string{
		"run",
		"--generator=run-pod/v1",
		name,
		"--rm",
		"-it",
		fmt.Sprintf("--namespace=%s", namespace),
		fmt.Sprintf("--image=%s", opts.ImageName),
		fmt.Sprintf("--limits=%s", limits),
		fmt.Sprintf("--requests=%s", requests),
		fmt.Sprintf("--labels=%s", strings.Join(labelSet, ",")),
		"--restart=Never",
		"--command",
		"--",
	}
	args = append(args, command...)

	cmd := exec.Command(bin, args...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	signCh := make(chan os.Signal, 1)
	errCh := make(chan error, 1)
	signal.Notify(signCh, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		errCh <- cmd.Run()
	}()

	select {
	case err := <-errCh:
		return err
	case <-signCh:
		err := DeletePod(opts.Namespace, &DeletePodOptions{
			Namespace: namespace,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
