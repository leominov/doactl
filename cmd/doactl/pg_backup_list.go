package main

import (
	"fmt"
	"io"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
)

type pgBackupListCmdOptions struct {
	backupListPrefix string
	exclude          []string
	latest           bool
}

func pgBackupListCmd(out io.Writer) *cobra.Command {
	opts := &pgBackupListCmdOptions{}

	cmd := &cobra.Command{
		Use: "list [flags]",
		Aliases: []string{
			"ls",
		},
		Short: "List backups",
		RunE: func(_ *cobra.Command, _ []string) error {
			return printsBackups(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.backupListPrefix, "prefix", CommonPrefix, "Filename prefix")
	cmd.Flags().StringSliceVar(&opts.exclude, "exclude", []string{}, "Exclude database from backups listing")
	cmd.Flags().BoolVar(&opts.latest, "latest", false, "Latest backups (will rewrite --date flag)")

	return cmd
}

func printsBackups(out io.Writer, opts *pgBackupListCmdOptions) error {
	d, err := backups.Open(BackupBucket)
	if err != nil {
		return err
	}
	defer d.Close()

	opt := &backups.ListOptions{
		Prefix: opts.backupListPrefix,
		Latest: opts.latest,
	}
	if len(BackupDate) > 0 {
		opt.Date = BackupDate
	}

	Spinner.Start()
	backups, err := d.List(opt)
	Spinner.Stop()
	if err != nil {
		return err
	}

	for i, backup := range backups {
		if matchToExcludedList(backup.Name, opts.exclude) {
			backups = append(backups[:i], backups[i+1:]...)
		}
	}

	if len(backups) == 0 {
		fmt.Fprintln(out, "<empty>")
		return nil
	}
	rows := make([]string, len(backups)+1)
	rows[0] = "Database|Backup ID|Size|Date"
	for i, backup := range backups {
		line := fmt.Sprintf("%s|%s|%s|%s", backup.Name, backup.File, humanize.Bytes(uint64(backup.Size)), backup.Date)
		rows[i+1] = line
	}
	if NoHeaders {
		rows = rows[1:]
	}
	fmt.Fprintln(out, formatList(rows))

	return nil
}
