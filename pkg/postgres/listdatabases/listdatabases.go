package listdatabases

import (
	"context"
	"fmt"
	"strings"

	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
)

// Options настройки операции получения списка баз данных
type Options struct {
	Prefix   string
	Instance string
	Database postgres.Database
}

func do(opt *Options) ([]string, error) {
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return nil, err
	}
	defer db.Close()
	rows, err := db.QueryContext(ctx, "SELECT datname FROM pg_database;")
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	databases := make([]string, 0)
	for rows.Next() {
		var dbname string
		if err := rows.Scan(&dbname); err != nil {
			return nil, err
		}
		if !strings.HasPrefix(dbname, opt.Prefix) {
			continue
		}
		databases = append(databases, dbname)
	}
	return databases, nil
}

// Do получить список баз данных инстанса
func Do(opt *Options) ([]string, error) {
	databases, err := do(opt)
	if err != nil {
		return nil, fmt.Errorf("list databases error: %v", err)
	}
	return databases, nil
}
