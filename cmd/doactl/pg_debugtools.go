package main

import (
	"fmt"
	"io"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/kubernetes"
)

type pgDebugToolsOptions struct {
	command   []string
	namespace string
	version   string
}

func pgDebugToolsCmd(out io.Writer) *cobra.Command {
	opts := &pgDebugToolsOptions{}

	cmd := &cobra.Command{
		Use: "debugtools",
		Aliases: []string{
			"debug",
			"debug-tools",
		},
		Short: "*Alpha* Starts POD in Kubernetes Cluster with doactl and PostgreSQL",
		RunE: func(_ *cobra.Command, args []string) error {
			opts.command = args
			return pgDebugTools(out, opts)
		},
	}

	cmd.Flags().StringVarP(&opts.namespace, "namespace", "n", "default", "Namespace")
	cmd.Flags().StringVarP(&opts.version, "version", "v", "9.6", "PostgreSQL version (9.6 or 11.5)")

	return cmd
}

func pgDebugTools(out io.Writer, opts *pgDebugToolsOptions) error {
	// Тег образа doactl состоит из двух версий - PostgreSQL и doactl, версия
	// PostgreSQL может быть задана через аргумента запуска, а версия doactl,
	// для совместимости, берем на основе той, через которую производим запуск,
	// переменная Version будет не пустой после сборки, туда будет передан тег
	version := "latest"
	if len(Version) > 0 {
		version = Version
	}

	server, err := kubernetes.CurrentContextServer()
	if err != nil {
		return err
	}
	registry := kubernetes.RegistryByClusterServer(server)
	imageName := fmt.Sprintf("%s/doactl:%s-%s", registry, opts.version, version)

	runPodOptions := &kubernetes.RunPodOptions{
		Command:   opts.command,
		ImageName: imageName,
		Namespace: opts.namespace,
	}

	podName := "doactl-" + time.Now().Format("200601021504")
	fmt.Fprintf(out, "Starting %q pod...\n", podName)
	return kubernetes.RunPod(podName, runPodOptions)
}
