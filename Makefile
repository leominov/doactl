.PHONY: test test-report

test:
	@echo ">> running tests"
	@go test -short ./... --cover

test-report:
	@echo ">> generating report"
	@go test -coverprofile=coverage.out ./...
	@go tool cover -html=coverage.out -o coverage.html
	@open coverage.html

