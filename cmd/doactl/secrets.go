package main

import (
	"io"

	"github.com/spf13/cobra"
)

func secretsCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "secrets",
		Short: "Manage secrets",
	}

	cmd.AddCommand(
		pgSecretsGetCmd(out),
	)

	return cmd
}
