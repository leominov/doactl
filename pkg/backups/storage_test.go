package backups

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"testing"

	_ "gocloud.dev/blob/fileblob"
)

func TestOpen(t *testing.T) {
	_, err := Open("not-found-directory")
	if err == nil {
		t.Error("Must be an error, byt got nil")
	}
	s, err := Open("file://./")
	if err != nil {
		t.Error(err)
	}
	s.Close()
}

func TestUpload(t *testing.T) {
	srcDir, err := ioutil.TempDir("", "storage_test_src")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(srcDir)
	destDir, err := ioutil.TempDir("", "storage_test_dest")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(destDir)
	err = ioutil.WriteFile(path.Join(srcDir, "upload_test_20060102.sql"), []byte("Hello"), os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	storage, err := Open(fmt.Sprintf("file://%s", destDir))
	if err != nil {
		t.Fatal(err)
	}
	_, err = storage.Upload(path.Join(srcDir, "upload_test_20060102.sql"), nil)
	if err != nil {
		t.Error(err)
	}
	_, err = storage.Info("upload_test_20060102.sql")
	if err != nil {
		t.Error(err)
	}
	_, err = storage.Exists("upload_test_20060102.sql")
	if err != nil {
		t.Error(err)
	}
	_, err = storage.List(&ListOptions{Prefix: "upload_test"})
	if err != nil {
		t.Error(err)
	}
	_, err = storage.List(nil)
	if err != nil {
		t.Error(err)
	}
	_, err = storage.List(&ListOptions{Date: "20060102"})
	if err != nil {
		t.Error(err)
	}
	_, err = storage.Upload(path.Join(srcDir, "not_found_file_20060102.sql"), nil)
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = storage.Info("not_found_file_20060102.sql")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	_, err = storage.Exists("not_found_file_20060102.sql")
	if err != nil {
		t.Error(err)
	}
}
