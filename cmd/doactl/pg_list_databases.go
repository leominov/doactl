package main

import (
	"fmt"
	"io"
	"strings"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/listdatabases"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgListDatabasesCmdOptions struct {
	instance       string
	user           string
	localhost      bool
	userListPath   string
	databasePrefix string
	force          bool
}

func pgListDatabasesCmd(out io.Writer) *cobra.Command {
	opts := &pgListDatabasesCmdOptions{}

	cmd := &cobra.Command{
		Use:   "listdatabases [flags]",
		Short: "List of databases",
		Aliases: []string{
			"listdb",
			"lsdb",
		},
		RunE: func(_ *cobra.Command, _ []string) error {
			return listDatabases(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opts.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().BoolVar(&opts.localhost, "local", false, "Rewrite settings to drop connections to database on localhost")
	cmd.Flags().StringVar(&opts.userListPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().StringVar(&opts.databasePrefix, "prefix", CommonPrefix, "Name prefix")
	cmd.Flags().BoolVar(&opts.force, "force", false, "Runs without confirmation")

	return cmd
}

func listDatabases(out io.Writer, opts *pgListDatabasesCmdOptions) error {
	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userListPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	listDatabasesOptions := &listdatabases.Options{
		Instance: opts.instance,
		Prefix:   opts.databasePrefix,
		Database: postgres.Database{
			User: user,
		},
	}

	Spinner.Start()
	dbnames, err := listdatabases.Do(listDatabasesOptions)
	Spinner.Stop()
	if err != nil {
		return err
	}

	if len(dbnames) > 0 {
		fmt.Fprintln(out, strings.Join(dbnames, "\n"))
	}

	return nil
}
