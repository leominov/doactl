package renamedatabase

import (
	"context"
	"fmt"

	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropconnections"
)

// Options настройки операции переименования базы данных
type Options struct {
	Instance string
	RenameTo string
	Database postgres.Database
}

// Do запустить операцию переименования базы данных
func Do(opt *Options) error {
	err := do(opt)
	if err != nil {
		return fmt.Errorf("rename database error: %v", err)
	}
	return nil
}

func do(opt *Options) error {
	dropConnectionOptions := &dropconnections.Options{
		Instance: opt.Instance,
		Database: opt.Database,
	}
	dropconnections.Do(dropConnectionOptions)
	database := opt.Database.Name
	// Удаляем имя базы данных, чтобы не использовать
	// её в строке подключения
	opt.Database.Name = ""
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return err
	}
	defer db.Close()
	const statement = `ALTER DATABASE "%s" RENAME TO "%s";`
	_, err = db.Exec(fmt.Sprintf(statement, database, opt.RenameTo))
	return err
}
