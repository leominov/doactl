package main

import (
	// Secrets
	_ "gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets/env"
	_ "gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets/vault"

	// Blobs
	// https://gocloud.dev/howto/blob/
	_ "gocloud.dev/blob/fileblob"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/blob/s3blob"

	// Databases
	// https://gocloud.dev/howto/sql/
	_ "gocloud.dev/postgres/gcppostgres"
)
