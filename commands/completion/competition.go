package completion

import (
	"io"

	"github.com/spf13/cobra"
)

// NewCmd возвращает подкоманды для формирования кода автокомплита
func NewCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "completion",
		Short: "Output shell completion code for the specified shell (bash or zsh)",
	}
	cmd.AddCommand(
		newBashCmd(out),
		newZshCmd(out),
	)
	return cmd
}
