package spinner

import (
	"os"
	"time"

	"github.com/briandowns/spinner"
	"golang.org/x/crypto/ssh/terminal"
)

// Spinner используется для индикации загрузки
type Spinner struct {
	spinner    *spinner.Spinner
	isTerminal bool
}

// New возвращает Spinner
func New(d time.Duration) *Spinner {
	sp := &Spinner{}
	sp.spinner = spinner.New(spinner.CharSets[11], d)
	sp.spinner.Writer = os.Stderr

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		sp.isTerminal = true
	}

	return sp
}

// Start запустить индикацию
func (s *Spinner) Start() {
	if !s.isTerminal {
		return
	}
	s.spinner.Start()
}

// Stop остановить индикацию
func (s *Spinner) Stop() {
	if !s.isTerminal {
		return
	}
	s.spinner.Stop()
}
