package main

import (
	"io"
	"os"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
)

var (
	// BackupBucket значение полученное из флага `--bucket`
	BackupBucket string
	// BackupDate значение полученное из флага `--date`
	BackupDate string
)

func pgBackupCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "backup",
		Short: "Interact with backups",
	}

	cmd.AddCommand(
		addBackupFlags(pgBackupListCmd(out)),
		addBackupFlags(pgBackupDownloadCmd(out)),
		addBackupFlags(pgBackupInfoCmd(out)),
		addBackupFlags(pgBackupRestoreCmd(out)),
		addBackupFlags(pgBackupCreateCmd(out)),
	)

	return cmd
}

func addBackupFlags(cmd *cobra.Command) *cobra.Command {
	currentDate := time.Now().Format(backups.BackupDateFilter)

	// Хранилище резервных копий баз данных. При использовании `file://` резервные
	// копии корректно создаются в текущем каталоге, при вызове команды `list` или
	// `info` резервные копии не удается найти, поэтому в качестве значения по
	// умолчанию используем текущий рабочий каталог пользователя.
	defaultBucket := "file://"
	workDir, err := os.Getwd()
	if err == nil {
		defaultBucket += workDir
	}

	cmd.Flags().StringVar(&BackupBucket, "bucket", defaultBucket, "Bucket URL (file:///path/to/dir/ or gs://my-bucket)")
	cmd.Flags().StringVar(&BackupDate, "date", currentDate, "Date for filtering")

	return cmd
}
