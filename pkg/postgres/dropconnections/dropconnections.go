package dropconnections

import (
	"context"
	"fmt"

	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
)

// Options настройки операции закрытия соединение
type Options struct {
	Instance string
	Database postgres.Database
}

func do(opt *Options) (int64, error) {
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return 0, err
	}
	defer db.Close()
	const statement = `SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE pid <> pg_backend_pid() AND datname = $1;`
	result, err := db.Exec(statement, opt.Database.Name)
	if err != nil {
		return 0, err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		// Не возвращаем ошибку, если не удалось получить число задетых строк
		return 0, nil
	}
	return rows, nil
}

// Do запустить операцию закрытия соединение и вернуть их
// число в случае успеха
//
// Число закрытых соединение может быть равно нулю в двух
// случаях:
// * не было активных соединений;
// * не удалось посчитать число закрытых соединений.
func Do(opt *Options) (int64, error) {
	n, err := do(opt)
	if err != nil {
		return n, fmt.Errorf("drop connections error: %v", err)
	}
	return n, nil
}
