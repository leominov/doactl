package psql

import (
	"fmt"
	"os/exec"
	"syscall"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type Options struct {
	Instance string
	Database string
	User     users.User
}

// Do запустить psql
func Do(opt *Options) error {
	err := do(opt)
	if err != nil {
		return fmt.Errorf("psql error: %v", err)
	}
	return nil
}

func do(opt *Options) error {
	psql, err := exec.LookPath("psql")
	if err != nil {
		return err
	}

	envs := []string{
		fmt.Sprintf("PGPASSWORD=%s", opt.User.Password),
	}

	args := []string{
		psql,
		"--no-password",
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.User.Name),
	}

	if len(opt.Database) > 0 {
		args = append(args, fmt.Sprintf("--dbname=%s", opt.Database))
	}

	// Stdin так же будет передан, можно использовать конструкции типа:
	// echo "SELECT NOW();" | doactl psql --local
	return syscall.Exec(psql, args, envs)
}
