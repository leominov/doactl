package completion

import (
	"io"

	"github.com/spf13/cobra"
)

func newBashCmd(out io.Writer) *cobra.Command {
	return &cobra.Command{
		Use:   "bash",
		Short: "Completion for Bash",
		RunE: func(cmd *cobra.Command, _ []string) error {
			return cmd.Root().GenBashCompletion(out)
		},
	}
}
