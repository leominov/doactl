package dropdatabase

import (
	"context"
	"fmt"

	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropconnections"
)

// Options настройки операции удаления базы данных
type Options struct {
	Instance string
	Database postgres.Database
}

func do(opt *Options) error {
	dropConnectionOptions := &dropconnections.Options{
		Instance: opt.Instance,
		Database: opt.Database,
	}
	dropconnections.Do(dropConnectionOptions)
	database := opt.Database.Name
	// Удаляем имя базы данных, чтобы не использовать
	// её в строке подключения
	opt.Database.Name = ""
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return err
	}
	defer db.Close()
	const statement = `DROP DATABASE IF EXISTS "%s";`
	_, err = db.Exec(fmt.Sprintf(statement, database))
	return err
}

// Do запустить операцию удаления базы данных
func Do(opt *Options) error {
	err := do(opt)
	if err != nil {
		return fmt.Errorf("drop database error: %v", err)
	}
	return nil
}
