package vault

import (
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/hashicorp/vault/api"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets"
)

// Vault реализует поддержку хранения секретов в HashiCorp Vault
type Vault struct {
	kvVersion string
	config    *Config
	client    *api.Client
}

func init() {
	v, err := NewVaultSecretEnv(nil)
	if err != nil {
		panic(err)
	}
	secrets.RegisterSecret("vault", v)
}

// NewVaultSecretEnv возвращает инициализированный инстанс Vault
// на основе переменных окружения из NewConfigFromEnv
func NewVaultSecretEnv(httpCli *http.Client) (*Vault, error) {
	c := NewConfigFromEnv()
	return NewVaultSecret(c, httpCli)
}

// NewVaultSecret возвращает инициализированный инстанс Vault, требуется
// передача конфигурации Config.
//
// При создании клиент для Vault (api.Client) не устанавливаем токен, так
// как в случае использования авторизационных бэкендов userpass или ldap
// это приведет к запросу к серверу Vault, при этом в ходе работы подпрограмм
// обращений к Vault может не быть
func NewVaultSecret(c *Config, httpCli *http.Client) (*Vault, error) {
	config := api.Config{
		Address:    c.Addr,
		HttpClient: httpCli,
	}
	client, err := api.NewClient(&config)
	if err != nil {
		return nil, err
	}

	return &Vault{
		kvVersion: c.KVVersion,
		config:    c,
		client:    client,
	}, nil
}

func addVersionizedFolder(path, name string) string {
	data := strings.SplitN(path, "/", 2)
	if len(data) != 2 {
		return path
	}

	resultMap := []string{data[0], name, data[1]}
	resultString := strings.Join(resultMap, "/")

	return resultString
}

func (v *Vault) setClientToken() error {
	if v.config.AuthMethod == "token" {
		v.client.SetToken(v.config.Token)
		return nil
	}
	options := map[string]interface{}{
		"password": v.config.Password,
	}
	path := fmt.Sprintf("auth/%s/login/%s", v.config.AuthMethod, v.config.User)
	secret, err := v.client.Logical().Write(path, options)
	if err != nil {
		return err
	}
	v.client.SetToken(secret.Auth.ClientToken)
	return nil
}

// ListPath получение списка секретов по указанному пути
func (v *Vault) ListPath(path string) ([]string, error) {
	var result []string

	err := v.setClientToken()
	if err != nil {
		return result, err
	}

	if v.kvVersion != "1" {
		path = addVersionizedFolder(path, "metadata")
	}

	secret, err := v.client.Logical().List(path)
	if err != nil {
		return result, err
	}
	if secret == nil {
		return result, fmt.Errorf("vault results empty for '%s'", path)
	}
	val, ok := secret.Data["keys"]
	if !ok {
		return result, errors.New("not found")
	}

	vOf := reflect.ValueOf(val)
	for i := 0; i < vOf.Len(); i++ {
		strct := vOf.Index(i).Interface()
		result = append(result, strct.(string))
	}

	return result, nil
}

// GetPath получение информации по указанному секрету
func (v *Vault) GetPath(path string) (map[string]string, error) {
	result := make(map[string]string)

	err := v.setClientToken()
	if err != nil {
		return result, err
	}

	if v.kvVersion != "1" {
		path = addVersionizedFolder(path, "data")
	}

	secret, err := v.client.Logical().Read(path)
	if err != nil {
		return result, err
	}
	if secret == nil || secret.Data == nil {
		return result, fmt.Errorf("vault results empty secret for '%s'", path)
	}

	var vOf reflect.Value
	if v.kvVersion != "1" {
		val, ok := secret.Data["data"]
		if !ok {
			return result, fmt.Errorf("vault results empty secret v2-data for '%s'", path)
		}
		vOf = reflect.ValueOf(val)
	} else {
		vOf = reflect.ValueOf(secret.Data)
	}

	keys := vOf.MapKeys()
	for _, key := range keys {
		keyStr := key.String()
		result[keyStr] = fmt.Sprintf("%s", vOf.MapIndex(key))
	}

	return result, nil
}
