package main

import (
	"io"

	"github.com/spf13/cobra"
)

func networkCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use: "network",
		Aliases: []string{
			"net",
		},
		Short: "Manage and debug network",
	}

	cmd.AddCommand(
		networkDebugToolsCmd(out),
	)

	return cmd
}
