package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropdatabase"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgDropDatabaseCmdOptions struct {
	instance     string
	database     string
	user         string
	localhost    bool
	userListPath string
	force        bool
}

func pgDropDatabaseCmd(out io.Writer) *cobra.Command {
	opts := &pgDropDatabaseCmdOptions{}

	cmd := &cobra.Command{
		Use:   "dropdatabase [flags] <database>",
		Short: "Drop database",
		Aliases: []string{
			"dropdb",
		},
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("database must be specified")
			}
			opts.database = args[0]
			return dropDatabase(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opts.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().BoolVar(&opts.localhost, "local", false, "Rewrite settings to drop connections to database on localhost")
	cmd.Flags().StringVar(&opts.userListPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().BoolVar(&opts.force, "force", false, "Runs without confirmation")

	return cmd
}

func dropDatabase(out io.Writer, opts *pgDropDatabaseCmdOptions) error {
	if !opts.force {
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Fprint(out, "Enter database name to drop: ")
		scanner.Scan()
		if scanner.Text() != opts.database {
			return errors.New("incorrect name")
		}
	}

	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userListPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	dropDatabaseOptions := &dropdatabase.Options{
		Instance: opts.instance,
		Database: postgres.Database{
			Name: opts.database,
			User: user,
		},
	}

	Spinner.Start()
	err := dropdatabase.Do(dropDatabaseOptions)
	Spinner.Stop()
	if err != nil {
		return err
	}

	fmt.Fprintln(out, "Dropped")

	return nil
}
