package main

import (
	"fmt"
	"io"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/kubernetes"
)

type networkDebugToolsOptions struct {
	command   []string
	namespace string
	version   string
}

func networkDebugToolsCmd(out io.Writer) *cobra.Command {
	opts := &networkDebugToolsOptions{}

	cmd := &cobra.Command{
		Use: "debugtools",
		Aliases: []string{
			"debug",
			"debug-tools",
		},
		Short: "*Alpha* Starts POD in Kubernetes Cluster with debugtools",
		RunE: func(_ *cobra.Command, args []string) error {
			opts.command = args
			return networkDebugTools(out, opts)
		},
	}

	cmd.Flags().StringVarP(&opts.namespace, "namespace", "n", "default", "Namespace")
	cmd.Flags().StringVarP(&opts.version, "version", "v", "latest", "Version of debugtools")

	return cmd
}

func networkDebugTools(out io.Writer, opts *networkDebugToolsOptions) error {
	server, err := kubernetes.CurrentContextServer()
	if err != nil {
		return err
	}
	registry := kubernetes.RegistryByClusterServer(server)
	imageName := fmt.Sprintf("%s/debugtools:%s", registry, opts.version)

	runPodOptions := &kubernetes.RunPodOptions{
		Command:   opts.command,
		ImageName: imageName,
		Namespace: opts.namespace,
	}

	podName := "debugtools-" + time.Now().Format("200601021504")
	fmt.Fprintf(out, "Starting %q pod...\n", podName)
	return kubernetes.RunPod(podName, runPodOptions)
}
