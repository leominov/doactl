package vault

import "os"

// Config конфигурация для инициализации Vault
type Config struct {
	Addr       string
	KVVersion  string
	User       string
	Password   string
	Token      string
	AuthMethod string
}

// NewConfigFromEnv получение конфигурации на основе
// переменных окружения
func NewConfigFromEnv() *Config {
	c := &Config{
		Addr:       os.Getenv("VAULT_ADDR"),
		KVVersion:  os.Getenv("VAULT_KV_VERSION"),
		User:       os.Getenv("VAULT_LOGIN"),
		Password:   os.Getenv("VAULT_PASSWORD"),
		Token:      os.Getenv("VAULT_TOKEN"),
		AuthMethod: os.Getenv("VAULT_METHOD"),
	}
	if len(c.KVVersion) == 0 {
		c.KVVersion = "2"
	}
	return c
}
