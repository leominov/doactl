package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/backup"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/listdatabases"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgBackupCreateCmdOptions struct {
	instance         string
	namePrefix       string
	userPath         string
	method           string
	schema           string
	databases        []string
	allDatabases     bool
	continueBackup   bool
	localhost        bool
	format           string
	excludeSchemas   []string
	excludeDatabases []string

	// Deprecated options
	skipCompareFactsDeprecated bool
	excludeDatabasesDeprecated []string
}

func pgBackupCreateCmd(out io.Writer) *cobra.Command {
	opt := &pgBackupCreateCmdOptions{}

	cmd := &cobra.Command{
		Use:   "create [flags] [database[s]]",
		Short: "Create database backup",
		RunE: func(_ *cobra.Command, args []string) error {
			opt.databases = args[0:]
			opt.processDeprecatedFlags()
			return createBackup(out, opt)
		},
	}

	cmd.Flags().StringVar(&opt.method, "method", "pg_dump", "Method of backup creation (pg_dump, pg_dumpall)")
	cmd.Flags().StringVar(&opt.schema, "schema", "", "Dump the named schema(s) only")
	cmd.Flags().BoolVar(&opt.allDatabases, "all", false, "Backup all databases defined in user list (work in couple with pg_dump)")
	cmd.Flags().StringVar(&opt.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opt.namePrefix, "prefix", CommonPrefix, "Name prefix")
	cmd.Flags().StringSliceVar(&opt.excludeDatabasesDeprecated, "exclude", []string{}, "*Deprecated* Exclude database from backup (use --exclude-database instead)")
	cmd.Flags().StringSliceVar(&opt.excludeDatabases, "exclude-database", []string{}, "Exclude database from backup")
	cmd.Flags().StringVar(&opt.userPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().BoolVar(&opt.continueBackup, "continue", false, "Continue if anyone backup was failed")
	cmd.Flags().BoolVar(&opt.localhost, "local", false, "Create backup of database on localhost")
	cmd.Flags().StringVar(&opt.format, "format", "custom", "*Beta* Backup file format (plain, custom)")
	cmd.Flags().BoolVar(&opt.skipCompareFactsDeprecated, "skip-compare-facts", false, "*Deprecated* Skip PostgreSQL version compare before backup creation")
	cmd.Flags().StringSliceVar(&opt.excludeSchemas, "exclude-schema", []string{}, "Do NOT dump the named schema(s)")

	cmd.Flags().MarkDeprecated("exclude", NextMajorDeprecatedNotice)
	cmd.Flags().MarkDeprecated("skip-compare-facts", NextMajorDeprecatedNotice)

	return cmd
}

func createPGDumpBackupSelected(out io.Writer, opts *pgBackupCreateCmdOptions, format backup.Format) error {
	if len(opts.databases) == 0 {
		return errors.New("database name must be specified")
	}

	var usersSelected []*users.User
	usersAll, err := users.List(opts.userPath, opts.namePrefix)
	if err != nil {
		return err
	}

	for _, database := range opts.databases {
		found := false
		for _, user := range usersAll {
			if user.Name != database {
				continue
			}
			found = true
			usersSelected = append(usersSelected, user)
		}
		if !found {
			return fmt.Errorf("user for %s database not found", database)
		}
	}

	failed := false
	for _, user := range usersSelected {
		match := matchToExcludedList(user.Name, opts.excludeDatabases)
		if match {
			continue
		}

		database := postgres.Database{
			Name: user.Name,
			User: user,
		}
		backupOpts := &backup.Options{
			Database:         database,
			Schema:           opts.schema,
			Bucket:           BackupBucket,
			Instance:         opts.instance,
			SkipCompareFacts: opts.skipCompareFactsDeprecated,
			Format:           format,
			ExcludeSchemas:   opts.excludeSchemas,
		}

		fmt.Fprintf(out, "Creating %s backup...\n", database.Name)

		Spinner.Start()
		b, err := backup.DoSingle(backupOpts)
		Spinner.Stop()
		if err != nil {
			if opts.continueBackup {
				failed = true
				fmt.Fprintln(out, err)
				continue
			}
			return err
		}

		rows := []string{
			fmt.Sprintf("Backup ID|%s", b.File),
			fmt.Sprintf("Date|%s", b.Date),
			fmt.Sprintf("ModTime|%s", b.ModTime),
			fmt.Sprintf("Size|%s", humanize.Bytes(uint64(b.Size))),
		}
		fmt.Fprintln(out, formatList(rows))
	}

	if failed {
		return errors.New("failed")
	}

	return nil
}

func createPGDumpAllBackup(out io.Writer, opts *pgBackupCreateCmdOptions) error {
	user, err := users.Get(opts.userPath, postgres.DefaultUser)
	if err != nil {
		return err
	}

	backupOpts := &backup.Options{
		Database: postgres.Database{
			User: user,
		},
		Schema:   opts.schema,
		Bucket:   BackupBucket,
		Instance: opts.instance,
	}

	fmt.Fprintf(out, "Creating %s backup...\n", backupOpts.Instance)

	Spinner.Start()
	b, err := backup.DoAll(backupOpts)
	Spinner.Stop()
	if err != nil {
		return err
	}

	rows := []string{
		fmt.Sprintf("Backup ID|%s", b.File),
		fmt.Sprintf("Date|%s", b.Date),
		fmt.Sprintf("ModTime|%s", b.ModTime),
		fmt.Sprintf("Size|%s", humanize.Bytes(uint64(b.Size))),
	}
	fmt.Fprintln(out, formatList(rows))

	return nil
}

func getUserListDatabases(userPath, namePrefix string) ([]postgres.Database, error) {
	var databases []postgres.Database

	users, err := users.List(userPath, namePrefix)
	if err != nil {
		return nil, err
	}

	for _, user := range users {
		db := postgres.Database{
			Name: user.Name,
			User: user,
		}
		databases = append(databases, db)
	}

	return databases, nil
}

func createPGDumpBackupAll(out io.Writer, opts *pgBackupCreateCmdOptions, format backup.Format) error {
	databases, err := getUserListDatabases(opts.userPath, opts.namePrefix)
	if err != nil {
		return err
	}

	failed := false
	for _, db := range databases {
		match := matchToExcludedList(db.Name, opts.excludeDatabases)
		if match {
			continue
		}

		backupOpts := &backup.Options{
			Database:       db,
			Schema:         opts.schema,
			Bucket:         BackupBucket,
			Instance:       opts.instance,
			Format:         format,
			ExcludeSchemas: opts.excludeSchemas,
		}

		fmt.Fprintf(out, "Creating %s backup...\n", backupOpts.Database.Name)

		Spinner.Start()
		b, err := backup.DoSingle(backupOpts)
		Spinner.Stop()
		if err != nil {
			if opts.continueBackup {
				failed = true
				fmt.Fprintln(out, err)
				continue
			}
			return err
		}

		rows := []string{
			fmt.Sprintf("Backup ID|%s", b.File),
			fmt.Sprintf("Date|%s", b.Date),
			fmt.Sprintf("ModTime|%s", b.ModTime),
			fmt.Sprintf("Size|%s", humanize.Bytes(uint64(b.Size))),
		}
		fmt.Fprintln(out, formatList(rows))
	}

	if failed {
		return errors.New("failed")
	}

	return nil
}

func createPGDumpBackupLocal(out io.Writer, opts *pgBackupCreateCmdOptions, format backup.Format) error {
	user := postgres.UserFromEnv()

	if opts.allDatabases {
		listDatabasesOptions := &listdatabases.Options{
			Instance: opts.instance,
			Prefix:   opts.namePrefix,
			Database: postgres.Database{
				User: user,
			},
		}
		dbNames, err := listdatabases.Do(listDatabasesOptions)
		if err != nil {
			return err
		}
		opts.databases = dbNames
	}

	if len(opts.databases) == 0 {
		return errors.New("database name must be specified")
	}

	failed := false
	for _, dbname := range opts.databases {
		match := matchToExcludedList(dbname, opts.excludeDatabases)
		if match {
			continue
		}

		backupOpts := &backup.Options{
			Database: postgres.Database{
				Name: dbname,
				User: user,
			},
			Schema:         opts.schema,
			Bucket:         BackupBucket,
			Instance:       opts.instance,
			Format:         format,
			ExcludeSchemas: opts.excludeSchemas,
		}

		fmt.Fprintf(out, "Creating %s backup...\n", backupOpts.Database.Name)

		Spinner.Start()
		b, err := backup.DoSingle(backupOpts)
		Spinner.Stop()
		if err != nil {
			if opts.continueBackup {
				failed = true
				fmt.Fprintln(out, err)
				continue
			}
			return err
		}

		rows := []string{
			fmt.Sprintf("Backup ID|%s", b.File),
			fmt.Sprintf("Date|%s", b.Date),
			fmt.Sprintf("ModTime|%s", b.ModTime),
			fmt.Sprintf("Size|%s", humanize.Bytes(uint64(b.Size))),
		}
		fmt.Fprintln(out, formatList(rows))
	}

	if failed {
		return errors.New("failed")
	}

	return nil
}

func createBackup(out io.Writer, opts *pgBackupCreateCmdOptions) error {
	format, err := backup.ParseFormat(opts.format)
	if err != nil {
		return err
	}
	if opts.localhost {
		if opts.method != "pg_dump" {
			return errors.New("flag `local` works only with `method=pg_dump` flag")
		}
		return createPGDumpBackupLocal(out, opts, format)
	}
	switch opts.method {
	case "pg_dump":
		if opts.allDatabases {
			return createPGDumpBackupAll(out, opts, format)
		}
		return createPGDumpBackupSelected(out, opts, format)
	case "pg_dumpall":
		return createPGDumpAllBackup(out, opts)
	}
	return errors.New("unsupported backup method")
}

// processDeprecatedFlags переносим значения из устаревающих флагов в актуальные
func (p *pgBackupCreateCmdOptions) processDeprecatedFlags() {
	if len(p.excludeDatabasesDeprecated) > 0 && len(p.excludeDatabases) == 0 {
		p.excludeDatabases = p.excludeDatabasesDeprecated
	}
}
