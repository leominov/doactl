package backups

import (
	"regexp"
	"time"

	"gocloud.dev/blob"
)

const (
	// BackupDateFilter формат даты используемый для фильтрации
	BackupDateFilter = "20060102"
	// BackupDateFormat формат даты используемый в именах резервных копий
	BackupDateFormat = "200601021504"
)

var (
	// BackupNameRegExp формат корректного имени файла резервной копии
	BackupNameRegExp = regexp.MustCompile(`([\w]+)_([0-9]{8,}).([\w]+)`)
	// BackupSuffixRegExp формат для выделения имени сервиса из имени файла
	// резервной копии
	BackupSuffixRegExp = regexp.MustCompile(`(_[0-9]{8,}.[\w]+)`)
)

// Backup структура описания резервной копии
type Backup struct {
	Self     string
	Name     string
	File     string
	Date     string
	Size     int64
	ModTime  time.Time
	Metadata map[string]string
}

func GetNameFromFile(file string) string {
	return BackupSuffixRegExp.ReplaceAllString(file, "")
}

// ListObjectToBackup получаем описание резервной копии на основе
// данных из blob.ListObject
func ListObjectToBackup(obj *blob.ListObject) *Backup {
	name := GetNameFromFile(obj.Key)
	return &Backup{
		Name:    name,
		File:    obj.Key,
		Date:    obj.ModTime.Format(BackupDateFormat),
		Size:    obj.Size,
		ModTime: obj.ModTime,
	}
}

// AttributesToBackup получаем описание резервной копии на основе
// данных из blob.Attributes
//
// blob.Attributes не возвращает Key, как это делает blob.ListObject,
// поэтому после вызова необходимо переопределить значение File.
func AttributesToBackup(attrs *blob.Attributes) *Backup {
	return &Backup{
		Date:     attrs.ModTime.Format(BackupDateFormat),
		Size:     attrs.Size,
		ModTime:  attrs.ModTime,
		Metadata: attrs.Metadata,
	}
}

// GetMetadata читает значение из Metadata по ключу, если ключ не найден
// будет возвращено пустое значение
func (b *Backup) GetMetadata(name string) string {
	v, ok := b.Metadata[name]
	if !ok {
		return ""
	}
	return v
}
