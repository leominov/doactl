package retry

import (
	"errors"
	"fmt"
	"math/rand"
	"time"
)

// Stats статистика работы retry
type Stats struct {
	Attempt   int
	Interval  time.Duration
	Config    *Config
	breakNext bool
}

// Config настройки retry
type Config struct {
	Maximum         int
	Interval        time.Duration
	Increment       bool
	IntervalMaximum time.Duration
	Forever         bool
	Jitter          bool
}

// String статистика строкой
func (s *Stats) String() string {
	str := fmt.Sprintf("Attempt %d/", s.Attempt)

	if s.Config.Forever {
		str = str + "∞"
	} else {
		str = str + fmt.Sprintf("%d", s.Config.Maximum)
	}

	if s.Config.Interval > 0 {
		str = str + fmt.Sprintf(" Retrying in %s", s.Interval)
	}

	return str
}

// Break остановка retry цикла
func (s *Stats) Break() {
	s.breakNext = true
}

// Do ретрай callback-функции
func Do(callback func(*Stats) error, config *Config) error {
	var err error

	// Дефолтные настройки, если ничего не было передано
	if config == nil {
		config = &Config{
			Forever:  true,
			Interval: 1 * time.Second,
			Jitter:   false,
		}
	}

	// Если ретрай не бесконечен, но не объявлен лимит попыток,
	// то устанавливаем сами
	if config.Maximum == 0 && !config.Forever {
		config.Maximum = 10
	}

	// Если есть инкремент интервала, но не задан максимум, то
	// устанавливаем сами
	if config.Increment && config.IntervalMaximum == 0 {
		config.IntervalMaximum = time.Minute
	}

	// Нельзя бесконечно ретраить без передачи интервала
	if config.Forever && config.Interval == 0 {
		return errors.New("you can't do a forever retry with no interval")
	}

	// Статистика, в которой хранится число попыток
	stats := &Stats{Attempt: 1, Config: config}

	// Нужно для рандомного смещенния
	random := rand.New(rand.NewSource(time.Now().UnixNano()))

	for {
		// Инкремент для интервала
		if config.Increment {
			stats.Interval = stats.Interval + config.Interval
		} else {
			stats.Interval = config.Interval
		}
		// Предел интервала по длительности
		if config.IntervalMaximum > 0 && stats.Interval > config.IntervalMaximum {
			stats.Interval = config.IntervalMaximum
		}
		// Случайное смещение в рамках 1 секунды
		if config.Jitter {
			stats.Interval = stats.Interval + (time.Duration(1000*random.Float32()) * time.Millisecond)
		}

		// Вызов пользовательского callback
		err = callback(stats)
		if err == nil {
			return nil
		}

		// Если был вызван stats.Break(), то завершаем работу
		if stats.breakNext {
			return err
		}

		// Инкремен числа попыток
		stats.Attempt = stats.Attempt + 1

		// Ожидаем заданный интервал
		time.Sleep(stats.Interval)

		// Если не бесконечный ретрай, то проверяем на лимит
		// попыток
		if !stats.Config.Forever && stats.Attempt > stats.Config.Maximum {
			break
		}
	}

	return err
}
