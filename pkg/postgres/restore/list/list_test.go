package list

import (
	"os/exec"
	"regexp"
	"strings"
	"testing"
)

func TestList_Filter(t *testing.T) {
	_, err := exec.LookPath("pg_restore")
	if err != nil {
		t.Skip("pg_restore must be installed")
	}
	list, err := Contents("test_data/test_20200313.sql")
	if err != nil {
		t.Fatal(err)
	}
	list.Filter(regexp.MustCompile("TABLE DATA public"))
	for _, line := range list.lines {
		if strings.Contains(line, "TABLE DATA public") {
			t.Error("Found unexpected string")
		}
	}
	list.Filter(regexp.MustCompile("TABLE public"))
	for _, line := range list.lines {
		if strings.Contains(line, "TABLE public") {
			t.Error("Found unexpected string")
		}
	}
}

func TestList_MassFilter(t *testing.T) {
	_, err := exec.LookPath("pg_restore")
	if err != nil {
		t.Skip("pg_restore must be installed")
	}
	list, err := Contents("test_data/test_20200313.sql")
	if err != nil {
		t.Fatal(err)
	}
	tests := []string{
		"TABLE DATA public",
		"TABLE public",
	}
	var res []*regexp.Regexp
	for _, test := range tests {
		res = append(res, regexp.MustCompile(test))
	}
	list.MassFilter(res)
	for _, line := range list.lines {
		for _, test := range tests {
			if strings.Contains(line, test) {
				t.Errorf("Found %q string", test)
			}
		}
	}
}

func TestContents(t *testing.T) {
	_, err := exec.LookPath("pg_restore")
	if err != nil {
		t.Skip("pg_restore must be installed")
	}
	_, err = Contents("test_data/not_found.sql")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	list, err := Contents("test_data/test_20200313.sql")
	if err != nil {
		t.Fatal(err)
	}
	if len(list.lines) == 0 {
		t.Error("Must be more that 0, but got nothing")
	}
	tests := []string{
		"; Archive created at 2020-03-13 21:53:18 +05",
		"196; 1259 18501 TABLE public test postgres",
		"2859; 0 18501 TABLE DATA public test postgres",
	}
	for _, test := range tests {
		match := false
		for _, line := range list.lines {
			if line == test {
				match = true
				break
			}
		}
		if !match {
			t.Errorf("String %q not found", test)
		}
	}
}
