package main

import (
	"fmt"
	"regexp"

	"github.com/ryanuber/columnize"
)

func formatList(in []string) string {
	columnConf := columnize.DefaultConfig()
	columnConf.Empty = "<none>"
	return columnize.Format(in, columnConf)
}

func matchToExcludedList(name string, list []string) bool {
	for _, dbExcluded := range list {
		if name == dbExcluded {
			return true
		}
	}
	return false
}

func parseSliceAsRegExp(sl []string) ([]*regexp.Regexp, error) {
	var res []*regexp.Regexp
	for _, s := range sl {
		re, err := regexp.Compile(s)
		if err != nil {
			return nil, fmt.Errorf("failed to process %q: %v", s, err)
		}
		res = append(res, re)
	}
	return res, nil
}
