package env

import (
	"os"
	"testing"
)

func TestListPath(t *testing.T) {
	e := &Env{}
	os.Unsetenv("USERS_leo")
	_, err := e.ListPath("USERS_")
	if err != nil {
		t.Errorf("ListPath() == %v", err)
	}
	os.Setenv("USERS_leo", "")
	_, err = e.ListPath("USERS_")
	if err != nil {
		t.Errorf("ListPath() == %v", err)
	}
	os.Setenv("USERS_leo", "eyJwYXNzd29yZCI6InBhc3N3b3JkIn0K")
	_, err = e.ListPath("USERS_")
	if err != nil {
		t.Errorf("ListPath() == %v", err)
	}
}

func TestGetPath(t *testing.T) {
	e := &Env{}
	os.Unsetenv("USERS_leo")
	_, err := e.GetPath("USERS_leo")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	os.Setenv("USERS_leo", "foobar!")
	_, err = e.GetPath("USERS_leo")
	if err == nil {
		t.Error("Must be an error, but got nil")
	}
	os.Setenv("USERS_leo", "eyJwYXNzd29yZCI6InBhc3N3b3JkIn0K")
	_, err = e.GetPath("USERS_leo")
	if err != nil {
		t.Errorf("GetPath() == %v", err)
	}
}
