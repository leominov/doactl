package main

import (
	"io"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/psql"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type psqlCmdOptions struct {
	instance  string
	user      string
	userPath  string
	localhost bool
}

func psqlCmd(out io.Writer) *cobra.Command {
	opt := &psqlCmdOptions{}

	cmd := &cobra.Command{
		Use:   "psql [flags] [database]",
		Short: "Open a psql shell to the database",
		RunE: func(_ *cobra.Command, args []string) error {
			database := ""
			if len(args) > 0 {
				database = args[0]
			}
			return openPSQL(out, opt, database)
		},
	}

	cmd.Flags().StringVar(&opt.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opt.userPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().StringVar(&opt.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().BoolVar(&opt.localhost, "local", false, "Connect to database on localhost")

	return cmd
}

func openPSQL(_ io.Writer, opts *psqlCmdOptions, database string) error {
	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	return psql.Do(&psql.Options{
		Instance: opts.instance,
		Database: database,
		User:     *user,
	})
}
