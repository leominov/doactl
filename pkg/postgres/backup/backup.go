package backup

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strconv"
	"time"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/facts"
)

const (
	// PGDumpBinary имя бинарного файла для запуска pg_dump
	PGDumpBinary = "pg_dump"
	// PGDumpCompressLevel уровень сжатия для pg_dump, может
	// быть от 0 до 9
	PGDumpCompressLevel = 6
	// PGDumpAllBinary имя бинарного файла для запуска pg_dumpall
	PGDumpAllBinary = "pg_dumpall"

	PGDumpFormatMeta           = "pgdumpformat"
	PGDumpCompressMeta         = "pgdumpcompress"
	PostgreSQLMajorVersionMeta = "postgresqlmajorversion"
	PostgreSQLVersionMeta      = "postgresqlversion"
	InstanceMeta               = "instance"
	SourceMeta                 = "source"
	TypeMeta                   = "type"
	EncryptionMeta             = "encryption"
)

var (
	// DefaultExcludeSchemas используется для игнорирования схем DWH
	DefaultExcludeSchemas = []string{
		// pg_dump: [archiver (db)] query failed: ERROR:  permission denied for schema dwh_ods_grant_manage
		// pg_dump: [archiver (db)] query was: LOCK TABLE dwh_ods_grant_manage.grant_select_on_table_to_ods_log IN ACCESS SHARE MODE
		"dwh_ods_grant_manage",
	}
)

// Options настройки операции создания резервной копии
type Options struct {
	Database         postgres.Database
	Schema           string
	Bucket           string
	Instance         string
	Format           Format
	SkipCompareFacts bool
	ExcludeSchemas   []string
}

// DoAll создает резервную копию через вызов утилиты pg_dumpall
func DoAll(opt *Options) (*backups.Backup, error) {
	bin, err := exec.LookPath(PGDumpAllBinary)
	if err != nil {
		return nil, err
	}

	factsLocal, err := facts.DoLocal()
	if err != nil {
		return nil, err
	}

	// Пытаемся подключиться к базе данных, убиваем при этом двух зайцев:
	// * проверяем подключение;
	// * получаем версию PostgreSQL для оценки совместимости.
	factsRemote, err := facts.DoRemove(&facts.Options{
		Instance: opt.Instance,
		Database: opt.Database,
	})
	if err != nil {
		return nil, err
	}

	if !opt.SkipCompareFacts {
		err = facts.CompareFacts(factsLocal, factsRemote)
		if err != nil {
			return nil, err
		}
	}

	storage, err := backups.Open(opt.Bucket)
	if err != nil {
		return nil, err
	}
	defer storage.Close()

	// Не можем писать сразу во Writer, поэтому складываем во
	// временный каталог из которого, в последующем переливаем
	// в указанный бакет
	tmpDir, err := ioutil.TempDir("", "backup")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(tmpDir)

	dumpFilename := path.Join(tmpDir,
		fmt.Sprintf("%s_%s.sql", opt.Instance, time.Now().Format(backups.BackupDateFormat)),
	)

	args := []string{
		"--no-owner",
		"--no-privileges",
		"--no-password",
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.Database.User.Name),
		fmt.Sprintf("--file=%s", dumpFilename),
		opt.Database.Name,
	}
	envs := []string{
		// (Лайф)хак, чтобы избежать передачи пароля в Stdin
		fmt.Sprintf("PGPASSWORD=%s", opt.Database.User.Password),
	}

	cmd := exec.Command(bin, args...)
	cmd.Env = envs
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return nil, err
	}

	return storage.Upload(dumpFilename, map[string]string{
		// Мажорная версия PostgreSQL будет использоваться для оценки
		// совместимости окружения, резервной копии и инстанса psql
		PostgreSQLMajorVersionMeta: strconv.Itoa(factsRemote.VersionMajor),
		PostgreSQLVersionMeta:      factsRemote.VersionRaw,
		InstanceMeta:               opt.Instance,
		SourceMeta:                 PGDumpAllBinary,
		TypeMeta:                   "all",
		EncryptionMeta:             "false",
	})
}

// DoSingle создает резервную копию через вызов утилиты pg_dump
func DoSingle(opt *Options) (*backups.Backup, error) {
	bin, err := exec.LookPath(PGDumpBinary)
	if err != nil {
		return nil, err
	}

	opt.ExcludeSchemas = append(opt.ExcludeSchemas, DefaultExcludeSchemas...)

	// Пытаемся подключиться к базе данных, убиваем при этом двух зайцев:
	// * проверяем подключение;
	// * получаем версию PostgreSQL для оценки совместимости.
	factsRemote, err := facts.DoRemove(&facts.Options{
		Instance: opt.Instance,
		Database: opt.Database,
	})
	if err != nil {
		return nil, err
	}

	factsLocal, err := facts.DoLocal()
	if err != nil {
		return nil, err
	}

	if !opt.SkipCompareFacts {
		err = facts.CompareFacts(factsLocal, factsRemote)
		if err != nil {
			return nil, err
		}
	}

	storage, err := backups.Open(opt.Bucket)
	if err != nil {
		return nil, err
	}

	tmpDir, err := ioutil.TempDir("", "backup")
	if err != nil {
		return nil, err
	}
	defer os.RemoveAll(tmpDir)

	dumpFilename := path.Join(tmpDir,
		fmt.Sprintf("%s_%s.sql", opt.Database.Name, time.Now().Format(backups.BackupDateFormat)),
	)

	args := []string{
		"--no-owner",
		"--no-acl",
		"--no-privileges",
		"--no-password",
		fmt.Sprintf("--host=%s", opt.Instance),
		fmt.Sprintf("--username=%s", opt.Database.User.Name),
		fmt.Sprintf("--file=%s", dumpFilename),
		fmt.Sprintf("--format=%s", opt.Format),
	}

	for _, schema := range opt.ExcludeSchemas {
		args = append(args, []string{"-N", schema}...)
	}

	if opt.Format == CustomFormat {
		args = append(args, fmt.Sprintf("--compress=%d", PGDumpCompressLevel))
	}

	args = append(args, opt.Database.Name)

	// Флаг `--no-tablespaces` не поддерживается на PostgreSQL 9.X, используем его только на
	// PostgreSQL версии 11 и выше, для корректного восстановления дампа 11 версии на 9.
	// Текст ошибки:
	// pg_restore: [archiver (db)] Error while PROCESSING TOC:
	// pg_restore: [archiver (db)] Error from TOC entry 3064; 1262 16555 DATABASE pfa_meetups postgres
	// pg_restore: [archiver (db)] could not execute query: ERROR:  tablespace "pfa_meetups" does not exist
	if factsRemote.VersionMajor >= 11 {
		args = append(args, "--no-tablespaces")
	}

	if opt.Schema != "" {
		args = append(args, fmt.Sprintf("--schema=%s", opt.Schema))
	}

	envs := []string{
		fmt.Sprintf("PGPASSWORD=%s", opt.Database.User.Password),
	}

	cmd := exec.Command(bin, args...)
	cmd.Env = envs
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return nil, err
	}

	meta := map[string]string{
		PGDumpFormatMeta:           opt.Format.String(),
		PostgreSQLMajorVersionMeta: strconv.Itoa(factsRemote.VersionMajor),
		PostgreSQLVersionMeta:      factsRemote.VersionRaw,
		InstanceMeta:               opt.Instance,
		SourceMeta:                 PGDumpBinary,
		TypeMeta:                   "single",
		EncryptionMeta:             "false",
	}

	if opt.Format == CustomFormat {
		meta[PGDumpCompressMeta] = strconv.Itoa(PGDumpCompressLevel)
	}

	return storage.Upload(dumpFilename, meta)
}
