package kubernetes

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestCurrentContextServer(t *testing.T) {
	tests := []struct {
		in      string
		out     string
		wantErr bool
	}{
		{
			wantErr: true,
		},
		{
			in:      `current-context: ABCD`,
			wantErr: true,
		},
		{
			in: `current-context: ABCD
contexts:
  - name: ABCD`,
			wantErr: true,
		},
		{
			in: `current-context: ABCD
contexts:
  - name: ABCD
    context:
      cluster: EFGH`,
			wantErr: true,
		},
		{
			in: `current-context: ABCD
contexts:
  - name: ABCD
    context:
      cluster: EFGH
clusters:
  - name: EFGH
    cluster:
      server: 127.0.0.1:8080`,
			out:     "127.0.0.1:8080",
			wantErr: false,
		},
	}
	for _, test := range tests {
		f, err := ioutil.TempFile("", "kubeconfig")
		if err != nil {
			t.Errorf("Failed to create temp file: %v", err)
			continue
		}
		os.Setenv("KUBECONFIG", f.Name())
		_, err = f.Write([]byte(test.in))
		server, err := CurrentContextServer()
		if test.wantErr != (err != nil) {
			t.Errorf("Must be %v, but got %v", test.wantErr, err != nil)
			continue
		}
		if test.out != server {
			t.Errorf("Must be %s, but got %s", test.out, server)
		}
	}
}

func TestRegistryByClusterServer(t *testing.T) {
	tests := map[string]string{
		"dex.prod.tcsbank.ru": internalRegistry,
		"127.0.0.1:9910":      externalRegistry,
		"api.gcr.io":          externalRegistry,
	}
	for in, out := range tests {
		res := RegistryByClusterServer(in)
		if res != out {
			t.Errorf("Must be %s, but got %s", out, res)
		}
	}
}
