package secrets

import (
	"fmt"
	"net/url"
	"strings"
)

var (
	// TODO(l.aminov): Переписать на gocloud.dev/internal/openurl
	secretEngines = map[string]Secret{}
)

// Secret интерфейс работы секретов, наверное, есть смысл
// переименовать в SecretStorage
//
// Поддержка тех или иных хранилищ секретов описывается в
// пакете main, например:
//
// import (
//   _ "gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets/vault"
// )
type Secret interface {
	ListPath(path string) ([]string, error)
	GetPath(path string) (map[string]string, error)
}

// RegisterSecret регистрация нового хранилища секретов
func RegisterSecret(name string, s Secret) {
	secretEngines[name] = s
}

func secretEngineByPath(path string) (Secret, string, error) {
	u, err := url.Parse(path)
	if err != nil {
		return nil, "", err
	}
	name := u.Scheme
	name = strings.ToLower(name)
	s, ok := secretEngines[name]
	if !ok {
		return nil, "", fmt.Errorf("unknown secret engine: %s", name)
	}
	path = strings.TrimPrefix(path, u.Scheme+"://")
	return s, path, nil
}

// ListPath получение списка секретов по указанному пути
//
// Путь должен содержать схему, указывающую на тип хранилища,
// например, vault://advert/pg/users
func ListPath(path string) ([]string, error) {
	s, p, err := secretEngineByPath(path)
	if err != nil {
		return nil, err
	}
	return s.ListPath(p)
}

// GetPath получение информации по указанному секрету
//
// Путь должен содержать схему, указывающую на тип хранилища,
// например, vault://advert/pg/users/postgres
func GetPath(path string) (map[string]string, error) {
	s, p, err := secretEngineByPath(path)
	if err != nil {
		return nil, err
	}
	return s.GetPath(p)
}
