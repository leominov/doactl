package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgCredentialsCmdOptions struct {
	database        string
	credentialsPath string
	userPrefix      string
	all             bool
}

func pgCredentialsCmd(out io.Writer) *cobra.Command {
	opts := &pgCredentialsCmdOptions{}

	cmd := &cobra.Command{
		Use:   "credentials [flags] <database>",
		Short: "Show information on credentials in the database",
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) > 0 {
				opts.database = args[0]
			}
			return credentials(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.credentialsPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().BoolVar(&opts.all, "all", false, "List of all users")
	cmd.Flags().StringVar(&opts.userPrefix, "prefix", CommonPrefix, "User prefix")

	return cmd
}

func credentialsSelected(out io.Writer, opts *pgCredentialsCmdOptions) error {
	if len(opts.database) == 0 {
		return errors.New("database must be specified")
	}

	Spinner.Start()
	user, err := users.Get(opts.credentialsPath, opts.database)
	Spinner.Stop()
	if err != nil {
		return err
	}

	rows := []string{
		fmt.Sprintf("Database|%s", opts.database),
		fmt.Sprintf("User|%s", user.Name),
		fmt.Sprintf("Password|%s", user.Password),
		fmt.Sprintf("Meta|%s", user.Meta),
	}
	fmt.Fprintln(out, formatList(rows))

	return nil
}

func credentialsAll(out io.Writer, opts *pgCredentialsCmdOptions) error {
	Spinner.Start()
	users, err := users.List(opts.credentialsPath, opts.userPrefix)
	Spinner.Stop()
	if err != nil {
		return err
	}

	if len(users) == 0 {
		fmt.Fprintln(out, "<empty>")
		return nil
	}

	rows := make([]string, len(users)+1)
	rows[0] = "User|Password|Meta"
	for i, user := range users {
		line := fmt.Sprintf("%s|%s|%s", user.Name, user.Password, user.Meta)
		rows[i+1] = line
	}
	if NoHeaders {
		rows = rows[1:]
	}
	fmt.Fprintln(out, formatList(rows))

	return nil
}

func credentials(out io.Writer, opts *pgCredentialsCmdOptions) error {
	if opts.all {
		return credentialsAll(out, opts)
	}
	return credentialsSelected(out, opts)
}
