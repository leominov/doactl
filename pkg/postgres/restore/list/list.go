package list

import (
	"bufio"
	"bytes"
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

type List struct {
	lines []string
}

func (l *List) String() string {
	return strings.Join(l.lines, "\n")
}

func (l *List) Bytes() []byte {
	return []byte(l.String())
}

func (l *List) Filter(re *regexp.Regexp) {
	var lines []string
	for _, line := range l.lines {
		if re.MatchString(line) {
			continue
		}
		lines = append(lines, line)
	}
	l.lines = lines
}

func (l *List) MassFilter(res []*regexp.Regexp) {
	for _, re := range res {
		l.Filter(re)
	}
}

func contents(backupFile string) ([]byte, error) {
	bin, err := exec.LookPath("pg_restore")
	if err != nil {
		return nil, err
	}
	args := []string{
		"--list",
		backupFile,
	}
	return exec.Command(bin, args...).CombinedOutput()
}

func Contents(backupFile string) (*List, error) {
	var lines []string
	b, err := contents(backupFile)
	if err != nil {
		return nil, fmt.Errorf("failed to get content: %v: %s", err, string(b))
	}
	scanner := bufio.NewScanner(bytes.NewReader(b))
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, strings.TrimSpace(line))
	}
	return &List{
		lines: lines,
	}, nil
}
