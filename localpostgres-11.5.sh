#!/usr/bin/env bash

set -uo pipefail

echo "Starting PostgreSQL Server..."
docker rm -f dev-postgres115 &> /dev/null || :
if ! docker run --cap-add=IPC_LOCK -d --name=dev-postgres115 -p 5432:5432 postgres:11.5 &> /dev/null ; then
  echo "Failed to start dev-postgres115"
  exit 1
fi
echo "...done. Run \"docker rm -f dev-postgres115\" to clean up the container."
echo
