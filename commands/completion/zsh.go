package completion

import (
	"io"

	"github.com/spf13/cobra"
)

func newZshCmd(out io.Writer) *cobra.Command {
	return &cobra.Command{
		Use:   "zsh",
		Short: "Completion for Zsh",
		RunE: func(cmd *cobra.Command, _ []string) error {
			return cmd.Root().GenZshCompletion(out)
		},
	}
}
