package backups

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path"
	"strings"

	"gocloud.dev/blob"
)

const attrsExt = ".attrs"

// Storage расширение для blob.Bucket
type Storage struct {
	bucketName string
	bucket     *blob.Bucket
}

// ListOptions опции листинга резервных копий
type ListOptions struct {
	Prefix string
	Date   string
	Latest bool
}

type Attributes struct {
	Size int64
	MD5  string
}

// Open получение нового инстанса Storage
func Open(bucketName string) (*Storage, error) {
	ctx := context.Background()
	bucket, err := blob.OpenBucket(ctx, bucketName)
	if err != nil {
		return nil, err
	}
	bucketName = strings.TrimSuffix(bucketName, "/")
	s := &Storage{
		bucketName: bucketName,
		bucket:     bucket,
	}
	return s, nil
}

// Upload заливает указанный файл в хранилище
func (s *Storage) Upload(targetFile string, meta map[string]string) (*Backup, error) {
	ctx := context.Background()

	r, err := os.Open(targetFile)
	if err != nil {
		return nil, err
	}
	defer r.Close()

	opts := &blob.WriterOptions{
		Metadata: meta,
	}
	w, err := s.bucket.NewWriter(ctx, path.Base(targetFile), opts)
	if err != nil {
		return nil, err
	}

	_, err = io.Copy(w, r)
	if err != nil {
		return nil, err
	}

	err = w.Close()
	if err != nil {
		return nil, err
	}

	attrs, err := s.bucket.Attributes(ctx, path.Base(targetFile))
	if err != nil {
		return nil, err
	}

	backup := AttributesToBackup(attrs)
	backup.File = path.Base(targetFile)

	return backup, nil
}

// Download скачивание резервной копии в указанный файл
func (s *Storage) Download(backupID, targetFile string) error {
	ctx := context.Background()

	attrs, err := s.bucket.Attributes(context.Background(), backupID)
	if err != nil {
		return err
	}
	attrFile, err := os.Create(targetFile + attrsExt)
	if err != nil {
		return err
	}
	defer attrFile.Close()

	fileAttrs := BlobAttributesToFileAttributes(attrs)
	err = json.NewEncoder(attrFile).Encode(fileAttrs)
	if err != nil {
		return err
	}

	reader, err := s.bucket.NewReader(ctx, backupID, nil)
	if err != nil {
		return err
	}
	defer reader.Close()

	outFile, err := os.Create(targetFile)
	if err != nil {
		return fmt.Errorf("failed to create output file %q: %v", targetFile, err)
	}
	defer outFile.Close()

	_, err = io.Copy(outFile, reader)
	if err != nil {
		return fmt.Errorf("failed to copy data: %v", err)
	}
	return nil
}

// Close завершение работы с инициализированным blob.Bucket
func (s *Storage) Close() {
	if s.bucket == nil {
		return
	}
	s.bucket.Close()
}

// Exists проверка существования резервной копии
func (s *Storage) Exists(backupID string) (bool, error) {
	ctx := context.Background()
	return s.bucket.Exists(ctx, backupID)
}

// Info информация о резервной копии
func (s *Storage) Info(backupID string) (*Backup, error) {
	attrs, err := s.bucket.Attributes(context.Background(), backupID)
	if err != nil {
		return nil, err
	}
	backup := AttributesToBackup(attrs)
	backup.Name = GetNameFromFile(backupID)
	backup.File = backupID
	backup.Self = s.bucketName + "/" + backupID
	return backup, nil
}

// List список резервных копий, фильтрация происходит на основе
// значений из ListOptions
func (s *Storage) List(opt *ListOptions) ([]*Backup, error) {
	if opt == nil {
		opt = &ListOptions{}
	}
	backupMap := make(map[string]*Backup)
	ctx := context.Background()
	blobOptions := &blob.ListOptions{
		Prefix: opt.Prefix,
	}
	iter := s.bucket.List(blobOptions)
	for {
		obj, err := iter.Next(ctx)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		if obj.IsDir {
			continue
		}
		if !BackupNameRegExp.MatchString(obj.Key) {
			continue
		}
		if len(opt.Date) > 0 && !opt.Latest && !strings.Contains(obj.Key, opt.Date) {
			continue
		}
		backup := ListObjectToBackup(obj)
		backup.Self = s.bucketName + "/" + backup.File
		backupMap[backup.Name] = backup
	}
	result := []*Backup{}
	for _, backup := range backupMap {
		result = append(result, backup)
	}
	return result, nil
}
