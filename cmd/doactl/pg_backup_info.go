package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
)

type pgBackupInfoCmdOptions struct {
	backupID string
}

func pgBackupInfoCmd(out io.Writer) *cobra.Command {
	opts := &pgBackupInfoCmdOptions{}

	cmd := &cobra.Command{
		Use:   "info [flags] <backup_id>",
		Short: "Get information about a specific backup",
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("backup ID must be specified")
			}
			opts.backupID = args[0]
			return infoBackup(out, opts)
		},
	}

	return cmd
}

func infoBackup(out io.Writer, opts *pgBackupInfoCmdOptions) error {
	d, err := backups.Open(BackupBucket)
	if err != nil {
		return err
	}
	defer d.Close()

	Spinner.Start()
	b, err := d.Info(opts.backupID)
	Spinner.Stop()
	if err != nil {
		return err
	}

	rows := []string{
		fmt.Sprintf("Backup ID|%s", b.File),
		fmt.Sprintf("Date|%s", b.Date),
		fmt.Sprintf("ModTime|%s", b.ModTime),
		fmt.Sprintf("Size|%s", humanize.Bytes(uint64(b.Size))),
	}
	for k, v := range b.Metadata {
		rows = append(rows, fmt.Sprintf("Meta > %s|%s", k, v))
	}
	fmt.Fprintln(out, formatList(rows))

	return nil
}
