package main

import (
	"database/sql"
	"errors"
	"fmt"
	"io"
	"regexp"
	"time"

	_ "github.com/lib/pq"
	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/restore"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgBackupRestoreCmdOptions struct {
	user            string
	owner           string
	userListPath    string
	localhost       bool
	instance        string
	backupDate      string
	database        string
	schema          string
	force           bool
	waitPeriod      int
	dropBefore      bool
	useTempDatabase bool
	useListExclude  []string
}

func pgBackupRestoreCmd(out io.Writer) *cobra.Command {
	opts := &pgBackupRestoreCmdOptions{}

	cmd := &cobra.Command{
		Use:   "restore [flags] <database>",
		Short: "Restore a database backup",
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("database must be specified")
			}
			opts.database = args[0]
			opts.backupDate = BackupDate
			return backupRestore(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opts.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().StringVar(&opts.owner, "owner", "", "Reassign database to specified user")
	cmd.Flags().BoolVar(&opts.localhost, "local", false, "Rewrite settings to restore database on localhost")
	cmd.Flags().StringVar(&opts.userListPath, "path", CommonUserList, "Path to user list")
	cmd.Flags().StringVar(&opts.schema, "schema", "public", "Schema to restore")
	cmd.Flags().BoolVar(&opts.force, "force", false, "Runs without waiting")
	cmd.Flags().BoolVar(&opts.dropBefore, "drop-before-restore", false, "Drop database if they exists before restoring")
	cmd.Flags().BoolVar(&opts.useTempDatabase, "use-temp-database", false, "Use temporary database for restoration procedure")
	cmd.Flags().IntVar(&opts.waitPeriod, "wait-period", 5, "Period of time in seconds given to wait before restoring data (only when --force if false)")
	cmd.Flags().StringSliceVar(&opts.useListExclude, "use-list-exclude", []string{}, "Exclude from table of content (based on \"pg_restore --list\")")

	return cmd
}

func backupRestore(out io.Writer, opts *pgBackupRestoreCmdOptions) error {
	var useListExclude []*regexp.Regexp

	if len(opts.useListExclude) > 0 {
		res, err := parseSliceAsRegExp(opts.useListExclude)
		if err != nil {
			return err
		}
		useListExclude = res
	}

	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userListPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	storage, err := backups.Open(BackupBucket)
	if err != nil {
		return err
	}
	defer storage.Close()

	backup, err := storage.Info(fmt.Sprintf("%s_%s.sql", opts.database, opts.backupDate))
	if err != nil {
		return err
	}

	if !opts.force && opts.waitPeriod > 0 {
		fmt.Fprintf(out, "Restoration of %s for %s will starts in %d seconds...\n", opts.database, opts.instance, opts.waitPeriod)
		Spinner.Start()
		time.Sleep(time.Duration(opts.waitPeriod) * time.Second)
		Spinner.Stop()
	}

	restoreOptions := &restore.Options{
		Bucket:          BackupBucket,
		Instance:        opts.instance,
		Backup:          backup,
		Owner:           opts.owner,
		DropBefore:      opts.dropBefore,
		UseTempDatabase: opts.useTempDatabase,
		Database: postgres.Database{
			Name: opts.database,
			User: user,
		},
		Schema:         opts.schema,
		UseListExclude: useListExclude,
	}

	Spinner.Start()
	err = restore.Do(restoreOptions)
	Spinner.Stop()
	if err != nil {
		return fmt.Errorf("failed to restore: %v", err)
	}

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
		opts.instance, user.Name, user.Password, opts.database))
	if err != nil {
		return fmt.Errorf("failed to connect: %v", err)
	}
	defer db.Close()

	_, err = db.Query(fmt.Sprintf("SET search_path = public"))
	if err != nil {
		return fmt.Errorf("failed to set search_path: %v", err)
	}
	_, err = db.Query(fmt.Sprintf("ALTER ROLE %s IN DATABASE %s SET search_path = public", user.Name, opts.database))
	if err != nil {
		return fmt.Errorf("failed to aler role: %v", err)
	}

	fmt.Fprintln(out, "Done")

	return nil
}
