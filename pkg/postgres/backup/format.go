package backup

import (
	"fmt"
)

// Format выходной формат для pg_dump, бывает нескольких видов:
// * p – Текстовый SQL-скрипт;
// * c – Архивный формате (для дальнейшего использования утилитой pg_restore);
// * d – В формате каталога;
// * t – В формате tar.
// У нас используется только два – тестовый и архивный (со сжатием). Формат со
// сжатием используется по умолчанию
type Format string

const (
	// CustomFormat – c – Архивный формате (для дальнейшего использования утилитой pg_restore);
	CustomFormat Format = "c"
	// PlainFormat – p – Текстовый SQL-скрипт
	PlainFormat Format = "p"
)

var (
	stringToFormatMap = map[string]Format{
		"custom": CustomFormat,
		"plain":  PlainFormat,
		// Для обратной совместимости со старыми резервными копиями, не рекомендуется использовать
		// в качестве значения для пользовательских флагов
		"c": CustomFormat,
		"p": PlainFormat,
	}
)

// ParseFormat по заданному имени формата возвращает репрезентацию типа Format
func ParseFormat(format string) (Format, error) {
	f, ok := stringToFormatMap[format]
	if !ok {
		return CustomFormat, fmt.Errorf("unknown backup format: %s", format)
	}
	return f, nil
}

func (f Format) String() string {
	return string(f)
}
