module gitlab-rnd.tcsbank.ru/devops/doactl

go 1.13

require (
	cloud.google.com/go/storage v1.1.1 // indirect
	github.com/GoogleCloudPlatform/cloudsql-proxy v0.0.0-20190828224159-d93c53a4824c // indirect
	github.com/aws/aws-sdk-go v1.25.10 // indirect
	github.com/briandowns/spinner v1.7.0
	github.com/dustin/go-humanize v1.0.0
	github.com/hashicorp/go-retryablehttp v0.6.2 // indirect
	github.com/hashicorp/go-version v1.2.0
	github.com/hashicorp/vault v1.2.3 // indirect
	github.com/hashicorp/vault/api v1.0.5-0.20190909201928-35325e2c3262
	github.com/lib/pq v1.2.0
	github.com/mattn/go-isatty v0.0.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pierrec/lz4 v2.3.0+incompatible // indirect
	github.com/prometheus/client_golang v1.1.0 // indirect
	github.com/prometheus/client_model v0.0.0-20190812154241-14fe0d1b01d4 // indirect
	github.com/prometheus/common v0.7.0 // indirect
	github.com/prometheus/procfs v0.0.5 // indirect
	github.com/prometheus/promu v0.5.0 // indirect
	github.com/ryanuber/columnize v2.1.0+incompatible
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.5 // indirect
	go.opencensus.io v0.22.1 // indirect
	gocloud.dev v0.17.0
	golang.org/x/crypto v0.0.0-20200220183623-bac4c82f6975
	golang.org/x/exp v0.0.0-20190927203820-447a159532ef // indirect
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/time v0.0.0-20190921001708-c4c64cad1fd0 // indirect
	golang.org/x/tools v0.0.0-20191001184121-329c8d646ebe // indirect
	google.golang.org/api v0.10.0 // indirect
	google.golang.org/appengine v1.6.4 // indirect
	google.golang.org/genproto v0.0.0-20190927181202-20e1ac93f88c // indirect
	google.golang.org/grpc v1.24.0 // indirect
	k8s.io/client-go v0.18.6
)
