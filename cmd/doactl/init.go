package main

import (
	"context"
	"encoding/base64"
	"io/ioutil"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"
	"gocloud.dev/gcp"
)

const (
	// storage-reader@utilities-212509.iam.gserviceaccount.com
	// Наблюдатель объектов в хранилище
	googleApplicationDefaultCredentials = `ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAidXRpbGl0aWVzLTIxMjUwOSIsCiAgInByaXZhdGVfa2V5X2lkIjogImE1YTg4MmEwZjEyZTgyZTQzMWUwYmIzN2YxMmI5Y2QwOGFkZDNjNjAiLAogICJwcml2YXRlX2tleSI6ICItLS0tLUJFR0lOIFBSSVZBVEUgS0VZLS0tLS1cbk1JSUV2QUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktZd2dnU2lBZ0VBQW9JQkFRRE9iK0l1S2ZOUjlWZkJcbkVFRGxZQm5JamdjOVlEbVlONVRRV0FaRWlmMG42SXUvMW9GOFZPSm1VQUFuU3Q2d2szTzhKSXlTMkVzWmtSeGVcbmNPOUJWeDFPblNwV3VoM3FRNUIzK0Nwck40RnF4aFQvbkxyMnhOeXc0TW5HeHg3enExOFNVMG4zZG0zK0MwY1FcbkVsTHVPUWwxU2VkdG1HUHIwVUtPZGw3aHM4MFp2ekJYcnBLZUtjSVhyVVI5YmY5MXdjMGp6cUx4TWFxVWMyWjRcbldEWlkwaXFtOGNIUktNZVBkNml0Q3o0N2JIdy9KV3N0WnpZY29kWmJZRCtFWXZaOEVKak1raUpOR3NUZWs2b0dcbjk5UzNScWx0UHQwVWhjZTlqNlpnUzNzMlRZUlZVVi9hNUd2OFhVVlpYREJ0MURlcS9uanJpbGlpU0U3dUhCUnlcblhQN1JOSXV4QWdNQkFBRUNnZ0VBRUtaRXdncGt5SGhzSjhoaXpqSjVBZUdLbUpyNUtqQ3V2T1doNDhjK29WMnlcblg3b3lNL3FOK1VoOGdBUWNLblpLQUU1R1ZDcnVWbFUySDRaN2xYUFc0TzlHRkpGQU5tai83dDBYWVdjQm9NQ2NcbmFFZnJhd1JTU2FXODZ3anc2SFdBTThuVWpsNkJNb1hBVTlNcjJIREY0QzEzWlRIRU9xY085M25PaWVUbTBZRDNcbnp2ZE9hSTN5dHBpc2lPOGZOWjhWV0dGWnhXbzV5V0ViSmZwenprOWM1OCtnNHQrYW9wa2EyTUlBdndjVDVQMmJcbkdaN1RBNnEwMVNLZmJFQzByR3hWbHk0TTUvOWlRWVdFT0xOdWQyb3RydlVXazl0MlU5VjhWeWZUSnhzeHBOelVcbjV3S3ZReTU2MUsyVFEzZUNaYitKYmx5UDVmUnJLeE5RS3BFU2RrRUQwUUtCZ1FENEV2anZpTzhqSGhaTWZhdEdcbkpobHg4RG53Mk9qQ080Z25VUEMvODI1bXN2cXF0ZTR0K2NiSzh6ZGhyalVSSW1GS0xJdmduYm9OOHltSjdzdm1cbk9KNlhPUVV5TXZlVGtVNGduY2MwUFBra2pEWU91Z3JzRXMwSE9nWjlFajNDN0lYQmFvWUl0YWRBdW1iem0wdW1cbmpVZmg4QW5OcnVYczkvd2hJQkJubXJlZXRRS0JnUURWQ0Z0S3ByaGZQa1BSdld5YWNJZUZPQURDR3U2L2hYdDhcbmkrdno5aklnb1FHUkFQUmRJWGJ2WExMT1Q1WUxPbVloNmFENVVnRnBkRHc4WlNySmo3WG9pMER0S1ZqMCthZTlcbklHMms2S0pKdUVQV0Fpc3hkYWVTVU9ZSWdrTkl0ait4QzNpREhvWXl0SW9UR24valJ5ektJZWpqQ28xYVVVQ01cbjNBZmM4THphalFLQmdGd0UvWjRzMXpmWmt6L3daYlh4c0FLWnI0UnRrWGxyQjhnOXQvQWI1bG5tRGlHQXRzY0xcbkhmaHVXNTY2TmlrUzdhL0w4bDd4WXZDRndHVFAxWWFpZmdGdVAwVXRyclJvYytpWlhpazhiTlpueEMyV1NUTGtcbmpuVWR5d1pkMjljUVVWVTdwRmhhdVozREJ5bFo4NmZtUkVIRS9uM3pZSXFVT2t3M1RwNS92NWo1QW9HQU5KVExcbmM3R1M4Rzg5N2hjREoxdC9GN0txL1Q0SWo5MHlqWnM3c2d1aWM4dEp4N0Nrb0JnU2N5SStRQTRFeXVDdk5NK2xcbm0rUlIxT1dMdUp4LzR5QkZsaEJKQmtDZVpGajZ2Z096REhFZkhYTU55MVRkdEdlRjVPZHkzOGJITkx0SEhUdm5cbmt2V0Z5aDRsdG5Gai80VWgyUUpscVNTQm56bE81WFdwdXk3d2JrRUNnWUJrYzBnbkxqQlJ2c2xacHVERVhkWWRcbldkM1FVTUlHMThxWjVCMWtEOHlHK0hzSVkydCs0dndVMEhmZEtTVXJ6eERPQXB4bm5PRmQyVC9rWkdITWhBYTJcbkMvTVVZbWM5aFc0b25DTUVnUG9INkdwS3VDN0dpeFBFVGJSbXJUN2E4UUwrRnVVSUJBWVBIWjNNMVhoK2w0SHhcbmhpSm1SbmlkMHRkRnlYZDVhdFpqTFE9PVxuLS0tLS1FTkQgUFJJVkFURSBLRVktLS0tLVxuIiwKICAiY2xpZW50X2VtYWlsIjogInN0b3JhZ2UtcmVhZGVyQHV0aWxpdGllcy0yMTI1MDkuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLAogICJjbGllbnRfaWQiOiAiMTE3MTQzNDMwNTg2MTgzMzIzMDA4IiwKICAiYXV0aF91cmkiOiAiaHR0cHM6Ly9hY2NvdW50cy5nb29nbGUuY29tL28vb2F1dGgyL2F1dGgiLAogICJ0b2tlbl91cmkiOiAiaHR0cHM6Ly9vYXV0aDIuZ29vZ2xlYXBpcy5jb20vdG9rZW4iLAogICJhdXRoX3Byb3ZpZGVyX3g1MDlfY2VydF91cmwiOiAiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vb2F1dGgyL3YxL2NlcnRzIiwKICAiY2xpZW50X3g1MDlfY2VydF91cmwiOiAiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vcm9ib3QvdjEvbWV0YWRhdGEveDUwOS9zdG9yYWdlLXJlYWRlciU0MHV0aWxpdGllcy0yMTI1MDkuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iCn0K`
	minIOAccessKey                      = `RjAwVk5JWkVEQ1QyR1lGQUJLWUo=`
	minIOSecretKey                      = `UzRYNHg4a0xwYlYrSFFIVFBGaFdVM0lUcHVFTXloRkg1OGhyY3ozag==`
)

type InitGCloudParams struct {
	WorkDir    string
	EncodedKey string
}

type InitMinIOParams struct {
	EncodedAccessKey string
	EncodedSecretKey string
}

// Initialize инициализация и настройка пользовательского окружение
func Initialize() error {
	dir, err := homedir.Dir()
	if err != nil {
		return err
	}
	// Если удалось получить данные из пользовательского окружения, то ничего не делаем
	_, err = gcp.DefaultCredentials(context.Background())
	if err != nil {
		err := initializeGCloud(&InitGCloudParams{
			WorkDir:    dir,
			EncodedKey: googleApplicationDefaultCredentials,
		})
		if err != nil {
			return err
		}
	}
	if len(os.Getenv("AWS_ACCESS_KEY_ID")) == 0 && len(os.Getenv("AWS_SECRET_ACCESS_KEY")) == 0 {
		err := initializeMinIO(&InitMinIOParams{
			EncodedAccessKey: minIOAccessKey,
			EncodedSecretKey: minIOSecretKey,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func initializeMinIO(initParams *InitMinIOParams) error {
	key, err := base64.StdEncoding.DecodeString(initParams.EncodedAccessKey)
	if err != nil {
		return err
	}
	if err := os.Setenv("AWS_ACCESS_KEY_ID", string(key)); err != nil {
		return err
	}
	key, err = base64.StdEncoding.DecodeString(initParams.EncodedSecretKey)
	if err != nil {
		return err
	}
	if err := os.Setenv("AWS_SECRET_ACCESS_KEY", string(key)); err != nil {
		return err
	}
	return nil
}

func initializeGCloud(initParams *InitGCloudParams) error {
	const envVar = "GOOGLE_APPLICATION_CREDENTIALS"
	credentialsDir := path.Join(initParams.WorkDir, ".config", "doactl")
	credentialsFilename := path.Join(credentialsDir, "application_default_credentials.json")
	if _, err := os.Stat(credentialsFilename); os.IsNotExist(err) {
		err := os.MkdirAll(credentialsDir, os.ModePerm)
		if err != nil {
			return err
		}
		key, err := base64.StdEncoding.DecodeString(initParams.EncodedKey)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(credentialsFilename, key, 0644)
		if err != nil {
			return err
		}
	}
	return os.Setenv(envVar, credentialsFilename)
}
