package main

import (
	"fmt"
	"io"
	"time"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/facts"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/retry"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgPingCmdOptions struct {
	user         string
	database     string
	instance     string
	userListPath string
	localhost    bool

	retry         bool
	retryAttempts int
	retryInterval time.Duration
}

func pgPingCmd(out io.Writer) *cobra.Command {
	opts := &pgPingCmdOptions{}

	cmd := &cobra.Command{
		Use:   "ping [flags]",
		Short: "Ping database",
		Aliases: []string{
			"facts",
		},
		RunE: func(_ *cobra.Command, _ []string) error {
			return pingDatabase(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opts.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().StringVar(&opts.database, "database", "", "Connect to specified database")
	cmd.Flags().BoolVar(&opts.localhost, "local", false, "Rewrite settings to restore database on localhost")
	cmd.Flags().StringVar(&opts.userListPath, "path", CommonUserList, "Path to user list")

	cmd.Flags().BoolVar(&opts.retry, "retry", false, "Enable retrying to connect")
	cmd.Flags().IntVar(&opts.retryAttempts, "retry-attempts", 5, "Number of attempts to connect")
	cmd.Flags().DurationVar(&opts.retryInterval, "retry-interval", 5*time.Second, "Interval between attempts to connect")

	return cmd
}

func pingDatabase(out io.Writer, opts *pgPingCmdOptions) error {
	var (
		infoRemote *facts.Facts
		errRemote  error
	)

	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userListPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	options := &facts.Options{
		Instance: opts.instance,
		Database: postgres.Database{
			Name: opts.database,
			User: user,
		},
	}

	if opts.retry {
		retryConfig := &retry.Config{
			Maximum:  opts.retryAttempts,
			Interval: opts.retryInterval,
		}
		errRemote = retry.Do(func(s *retry.Stats) error {
			fmt.Fprintln(out, s.String())
			info, err := facts.DoRemove(options)
			if err != nil {
				return err
			}
			infoRemote = info
			return nil
		}, retryConfig)
	} else {
		Spinner.Start()
		infoRemote, errRemote = facts.DoRemove(options)
		Spinner.Stop()
	}
	if errRemote != nil {
		return errRemote
	}

	versionLocal := "?"
	infoLocal, errLocal := facts.DoLocal()
	if errLocal == nil {
		versionLocal = infoLocal.VersionRaw
	}

	rows := []string{
		fmt.Sprintf("Instance|%s", opts.instance),
		fmt.Sprintf("Database|%s", opts.database),
		fmt.Sprintf("User|%s", opts.user),
		fmt.Sprintf("Full Version|%s (local=%s)", infoRemote.VersionRaw, versionLocal),
		fmt.Sprintf("Major Version|%d", infoRemote.VersionMajor),
		fmt.Sprintf("Minor Version|%d", infoRemote.VersionMinor),
	}
	fmt.Fprintln(out, formatList(rows))

	if errLocal != nil {
		return fmt.Errorf("failed to get local version: %v", errLocal)
	}

	err := facts.CompareFacts(infoLocal, infoRemote)
	if err != nil {
		return err
	}

	return nil
}
