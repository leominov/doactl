package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/backups"
)

type pgBackupDownloadCmdOptions struct {
	backupID string
}

func pgBackupDownloadCmd(out io.Writer) *cobra.Command {
	opts := &pgBackupDownloadCmdOptions{}

	cmd := &cobra.Command{
		Use:   "download [flags] <backup_id>",
		Short: "Downloads database backup",
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("backup ID must be specified")
			}
			opts.backupID = args[0]
			return downloadBackup(out, opts)
		},
	}

	return cmd
}

func downloadBackup(out io.Writer, opts *pgBackupDownloadCmdOptions) error {
	d, err := backups.Open(BackupBucket)
	if err != nil {
		return err
	}
	defer d.Close()

	Spinner.Start()
	err = d.Download(opts.backupID, opts.backupID)
	Spinner.Stop()
	if err != nil {
		return err
	}
	fmt.Fprintf(out, "Saved to %s\n", opts.backupID)

	return nil
}
