package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres/dropconnections"
	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/users"
)

type pgDropConnectionsCmdOptions struct {
	instance     string
	database     string
	user         string
	localhost    bool
	userListPath string
}

func pgDropConnectionsCmd(out io.Writer) *cobra.Command {
	opts := &pgDropConnectionsCmdOptions{}

	cmd := &cobra.Command{
		Use:   "dropconnections [flags] <database>",
		Short: "Drop connections to database",
		Aliases: []string{
			"dropconns",
		},
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) == 0 {
				return errors.New("database must be specified")
			}
			opts.database = args[0]
			return dropConnections(out, opts)
		},
	}

	cmd.Flags().StringVar(&opts.instance, "instance", CommonInstance, "Database instance")
	cmd.Flags().StringVar(&opts.user, "user", postgres.DefaultUser, "Connect as specified database user")
	cmd.Flags().BoolVar(&opts.localhost, "local", false, "Rewrite settings to drop connections to database on localhost")
	cmd.Flags().StringVar(&opts.userListPath, "path", CommonUserList, "Path to user list")

	return cmd
}

func dropConnections(out io.Writer, opts *pgDropConnectionsCmdOptions) error {
	user := postgres.UserFromEnv()

	if !opts.localhost {
		userSecured, err := users.Get(opts.userListPath, opts.user)
		if err != nil {
			return err
		}
		user = userSecured
	}

	dropConnectionOptions := &dropconnections.Options{
		Instance: opts.instance,
		Database: postgres.Database{
			Name: opts.database,
			User: user,
		},
	}

	Spinner.Start()
	n, err := dropconnections.Do(dropConnectionOptions)
	Spinner.Stop()
	if err != nil {
		return err
	}

	fmt.Fprintf(out, "Dropped connections: %d\n", n)

	return nil
}
