package kubernetes

import (
	"fmt"
	"strings"

	"k8s.io/client-go/tools/clientcmd"
)

const (
	internalRegistry = "docker-internal.tcsbank.ru/advert-devops"
	externalRegistry = "eu.gcr.io/utilities-212509/devops"
)

func RegistryByClusterServer(server string) string {
	registry := externalRegistry
	if strings.Contains(server, "tcsbank.ru") {
		registry = internalRegistry
	}
	return registry
}

func CurrentContextServer() (string, error) {
	opts := clientcmd.NewDefaultPathOptions()
	config, err := opts.GetStartingConfig()
	if err != nil {
		return "", err
	}
	context, ok := config.Contexts[config.CurrentContext]
	if !ok {
		return "", fmt.Errorf("context %s not found", config.CurrentContext)
	}
	cluster, ok := config.Clusters[context.Cluster]
	if !ok {
		return "", fmt.Errorf("cluster %s not found", context.Cluster)
	}
	return cluster.Server, nil
}
