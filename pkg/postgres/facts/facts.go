package facts

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"os/exec"
	"strings"
	"time"

	"github.com/hashicorp/go-version"
	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
)

// PSQLBinary имя бинарного файла для запуска psql
const PSQLBinary = "psql"

// Options настройки операции получения фактов
type Options struct {
	Database postgres.Database
	Instance string
}

// Facts описание сведенний о базе данных
type Facts struct {
	VersionRaw string
	// VersionMajor не можем утверждать, что разные мажорные версии
	// PostgreSQL совместимы между собой, поэтому для сравнения
	// используется именно этот параметр
	VersionMajor int
	VersionMinor int
}

func collect(ctx context.Context, db *sql.DB) (*Facts, error) {
	err := db.Ping()
	if err != nil {
		return nil, err
	}
	var versionRaw string
	err = db.QueryRowContext(ctx, "SELECT version();").Scan(&versionRaw)
	if err != nil {
		return nil, err
	}
	versionData := strings.Split(versionRaw, " ")
	if len(versionData) < 2 {
		return nil, errors.New("Failed to parse version")
	}
	sv, err := version.NewVersion(versionData[1])
	if err != nil {
		return nil, err
	}
	facts := &Facts{
		VersionRaw: sv.String(),
	}
	segments := sv.Segments()
	if len(segments) > 0 {
		facts.VersionMajor = segments[0]
	}
	if len(segments) > 1 {
		facts.VersionMinor = segments[1]
	}
	return facts, nil
}

// DoRemove запустить операцию получения информации о базе данных
func DoRemove(opt *Options) (*Facts, error) {
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return nil, err
	}
	defer db.Close()
	return collect(ctx, db)
}

func getPostgreSQLVersion(bin string) (*version.Version, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	b, err := exec.CommandContext(ctx, bin, "--version").CombinedOutput()
	if err != nil {
		return nil, err
	}
	// https://github.com/postgres/postgres/blob/REL_10_0/src/include/port.h#L102
	// https://github.com/postgres/postgres/blob/REL_11_STABLE/src/include/port.h#L111
	data := strings.Split(string(b), " ")
	if len(data) < 3 {
		return nil, errors.New("Failed to get PostgreSQL version")
	}
	psqlVersion := strings.TrimSpace(data[2])
	return version.NewVersion(psqlVersion)
}

// DoLocal запустить операцию получения информации о локальном окружении
func DoLocal() (*Facts, error) {
	bin, err := exec.LookPath(PSQLBinary)
	if err != nil {
		return nil, err
	}
	v, err := getPostgreSQLVersion(bin)
	if err != nil {
		return nil, err
	}
	segments := v.Segments()
	facts := &Facts{
		VersionRaw: v.String(),
	}
	if len(segments) > 0 {
		facts.VersionMajor = segments[0]
	}
	if len(segments) > 1 {
		facts.VersionMinor = segments[1]
	}
	return facts, nil
}

// CompareFacts сравнение совместимости окружений.
func CompareFacts(a, b *Facts) error {
	// Считаем, что нет гарантии корректной работы, если мажорные
	// версии PostgreSQL различаются
	if a.VersionMajor != b.VersionMajor {
		return fmt.Errorf("PostgreSQL version %s not equal to %s; major version must the same", a.VersionRaw, b.VersionRaw)
	}
	return nil
}
