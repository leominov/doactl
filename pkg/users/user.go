package users

const (
	// NameKey зарезервировано под имя пользователя
	//
	// Используется в случае, когда имя ключа не является
	// именем пользователя
	NameKey = "name"

	// PasswordKey зарезервировано под пароль пользователя
	//
	// Поле PasswordKey обязательно при получении информации
	// о пользователей
	PasswordKey = "password"
)

// User структура пользователя, по которой будет возвращаться
// список пользователей
type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`

	// Meta содержит всю сопутствующую информацию полученную
	// из хранилища, за исключением пароля
	Meta map[string]string `json:"meta"`
}
