package reassign

import (
	"context"
	"database/sql"
	"fmt"

	psql "gocloud.dev/postgres"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/postgres"
)

const (
	// DatabaseAlterStatementTemplate меняем владельца всей базы данных
	DatabaseAlterStatementTemplate = `ALTER DATABASE "%s" OWNER TO "%s";`
	// AlterStatementsTemplate используется при формировании запросов,
	// полученных в результате выполнения AlterStatements
	AlterStatementsTemplate = `ALTER %s %s."%s" OWNER TO "%s";`
)

var (
	// BasicStatements базовые стейтменты. На текущий момент содержит
	// только Grant All
	BasicStatements = []string{
		`GRANT ALL ON SCHEMA public TO PUBLIC;`,
	}
	// AlterStatements содержит генераторы Alter table|sequence|view для
	// смены владельца у сущностей внутри базы данных
	AlterStatements = []string{
		`SELECT 'TABLE' as atype, schemaname, tablename FROM pg_tables WHERE NOT schemaname IN ('pg_catalog', 'information_schema') ORDER BY schemaname, tablename;`,
		`SELECT 'SEQUENCE' as atype, sequence_schema, sequence_name FROM information_schema.sequences WHERE NOT sequence_schema IN ('pg_catalog', 'information_schema') ORDER BY sequence_schema, sequence_name;`,
		`SELECT 'VIEW' as atype, table_schema, table_name FROM information_schema.views WHERE NOT table_schema IN ('pg_catalog', 'information_schema') ORDER BY table_schema, table_name;`,
	}
)

// Options настройки операции смены владельца
type Options struct {
	Instance string
	Owner    string
	Database postgres.Database
}

// Do запустить операцию смены владельца базы данных
func Do(opt *Options) error {
	err := do(opt)
	if err != nil {
		return fmt.Errorf("reassign error: %v", err)
	}
	return nil
}

func do(opt *Options) error {
	ctx := context.Background()
	u := opt.Database.URL(opt.Instance)
	db, err := psql.Open(ctx, u.String())
	if err != nil {
		return err
	}
	defer db.Close()
	databaseAlterStatement := fmt.Sprintf(DatabaseAlterStatementTemplate, opt.Database.Name, opt.Owner)
	basicStatements := append(BasicStatements, databaseAlterStatement)
	for _, statement := range basicStatements {
		_, err = db.ExecContext(ctx, statement)
		if err != nil {
			return err
		}
	}
	for _, statement := range AlterStatements {
		err := execAlterStatements(ctx, db, statement, opt.Owner)
		if err != nil {
			return err
		}
	}
	return nil
}

func execAlterStatements(ctx context.Context, db *sql.DB, statement, owner string) error {
	var (
		aType  string
		scheme string
		entry  string
	)
	q, err := db.QueryContext(ctx, statement)
	if err != nil {
		return err
	}
	defer q.Close()
	for q.Next() {
		if err := q.Scan(&aType, &scheme, &entry); err != nil {
			return err
		}
		db.ExecContext(ctx, fmt.Sprintf(
			AlterStatementsTemplate, aType, scheme, entry, owner,
		))
	}
	return nil
}
