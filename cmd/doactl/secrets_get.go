package main

import (
	"errors"
	"fmt"
	"io"

	"github.com/spf13/cobra"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets"
)

const (
	secretsGetExample = `  doactl secrets get vault://advert/ansible/infra-devops/common key`
)

func pgSecretsGetCmd(out io.Writer) *cobra.Command {
	cmd := &cobra.Command{
		Use:     "get <path> <field>",
		Example: secretsGetExample,
		Short:   "Display a secret value",
		RunE: func(_ *cobra.Command, args []string) error {
			if len(args) != 2 {
				return errors.New("path and field must be specified")
			}
			return secretGet(out, args[0], args[1])
		},
	}
	return cmd
}

func secretGet(out io.Writer, secretPath, field string) error {
	data, err := secrets.GetPath(secretPath)
	if err != nil {
		return err
	}
	v, ok := data[field]
	if !ok {
		return fmt.Errorf("field %q not found", field)
	}
	fmt.Fprint(out, v)
	return nil
}
