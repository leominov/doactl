package users

import (
	"fmt"
	"strings"

	"gitlab-rnd.tcsbank.ru/devops/doactl/pkg/secrets"
)

// Get получение пользователя по пути и имени ключа из хранилища секретов
func Get(p string, key string) (*User, error) {
	p = strings.TrimSuffix(p, "/")
	pathFull := p + "/" + key
	data, err := secrets.GetPath(pathFull)
	if err != nil {
		return nil, err
	}
	password, ok := data[PasswordKey]
	if !ok {
		return nil, fmt.Errorf("password not found in %s path", pathFull)
	}
	u := &User{
		Name:     key,
		Password: password,
	}
	name, ok := data[NameKey]
	if ok {
		u.Name = name
	}
	delete(data, PasswordKey)
	u.Meta = data
	return u, nil
}

// List получение списка пользователей по пути и префиксу из хранилища секретов
func List(p string, pref string) ([]*User, error) {
	keys, err := secrets.ListPath(p)
	if err != nil {
		return nil, err
	}
	users := []*User{}
	for _, key := range keys {
		if !strings.HasPrefix(key, pref) {
			continue
		}
		user, err := Get(p, key)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}
